use std::str::FromStr;

use crate::entities::constants::{F64_DELTA, MEM_GB, MEM_KB, MEM_MB};

#[derive(Clone, Debug)]
pub enum KillReason {
    TempGt(f64),
    ProcMemGt(u64),
    FreeMemLt(u64),
}

impl Eq for KillReason {}

impl PartialEq for KillReason {
    fn eq(&self, other: &Self) -> bool {
        use KillReason::*;

        match (self, other) {
            (TempGt(a), TempGt(b)) => (a - b).abs() < F64_DELTA,
            (a, b) => format!("{:?}", a) == format!("{:?}", b),
        }
    }
}

fn parse_val<T: FromStr>(src: &str, prefix: &str) -> Result<T, ()> {
    T::from_str(&src[prefix.len()..]).map_err(|_| ())
}

fn parse_val_f<T, E>(
    src: &str,
    prefix: &str,
    func: impl Fn(&str) -> Result<T, E>,
) -> Result<T, ()> {
    func(&src[prefix.len()..]).map_err(|_| ())
}

fn parse_mem(src: &str) -> Result<u64, ()> {
    fn parse_no_suffix(src: &str, suffix: &str) -> Result<u64, ()> {
        match src.split(suffix).collect::<Vec<_>>()[..] {
            [num, ""] => u64::from_str(num).map_err(|_| ()),
            _ => Err(()),
        }
    }

    Ok(match () {
        _ if src.ends_with("kb") => parse_no_suffix(src, "kb")? * MEM_KB,
        _ if src.ends_with("mb") => parse_no_suffix(src, "mb")? * MEM_MB,
        _ if src.ends_with("gb") => parse_no_suffix(src, "gb")? * MEM_GB,
        _ if src.ends_with('b') => parse_no_suffix(src, "b")?,
        _ => u64::from_str(src).map_err(|_| ())?,
    })
}

impl KillReason {
    const TEMP: &'static str = "temp>";
    const PROC_MEM: &'static str = "proc_mem>";
    const FREE_MEM: &'static str = "free_mem<";
}

impl FromStr for KillReason {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let replaced = s.replace(' ', "").to_lowercase();

        Ok(match () {
            _ if replaced.starts_with(Self::TEMP) => {
                Self::TempGt(parse_val(&replaced, Self::TEMP)?)
            }
            _ if replaced.starts_with(Self::PROC_MEM) => {
                Self::ProcMemGt(parse_val_f(&replaced, Self::PROC_MEM, parse_mem)?)
            }
            _ if replaced.starts_with(Self::FREE_MEM) => {
                Self::FreeMemLt(parse_val_f(&replaced, Self::FREE_MEM, parse_mem)?)
            }
            _ => return Err(()),
        })
    }
}

#[derive(Default, Debug, PartialEq, Eq)]
pub struct ServiceConfig {
    pub name: String,
    pub command: String,
    pub start: bool,
    pub kill_on: Vec<KillReason>,
    pub vars: Vec<(String, String)>,
}

#[derive(Debug)]
pub struct SocketConfig {
    pub path: String,
    pub timeout: f64,
}

impl Eq for SocketConfig {}

impl PartialEq for SocketConfig {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path && (self.timeout - other.timeout).abs() < F64_DELTA
    }
}

#[derive(Debug, Clone)]
pub struct TelegramConfig {
    pub admin_id: i64,
    pub token: String,
    pub notification_interval: f64,
    pub allow_shell: bool,
    pub allow_loading: bool,
}

impl Eq for TelegramConfig {}

impl PartialEq for TelegramConfig {
    fn eq(&self, other: &Self) -> bool {
        self.admin_id == other.admin_id
            && self.token == other.token
            && (self.notification_interval - other.notification_interval).abs() < F64_DELTA
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum IOConfig {
    Socket(SocketConfig),
    Tg(TelegramConfig),
}

#[derive(Debug, PartialEq, Eq)]
pub enum NotificatorConfig {
    Tg(TelegramConfig),
}

#[derive(Debug)]
pub enum NotificationKind {
    TempGt(f64),
    FreeMemLt(u64),
    CpuUsageGt(f64),
    ProcMemGt(String, u64),
    ServiceKilled,
    Start,
    BusError,
    NoData,
}

impl Eq for NotificationKind {}

impl PartialEq for NotificationKind {
    fn eq(&self, other: &Self) -> bool {
        use NotificationKind::*;

        match (self, other) {
            (TempGt(a), TempGt(b)) => (a - b).abs() < F64_DELTA,
            (CpuUsageGt(a), CpuUsageGt(b)) => (a - b).abs() < F64_DELTA,
            (a, b) => format!("{:?}", a) == format!("{:?}", b),
        }
    }
}

impl NotificationKind {
    const TEMP: &'static str = "temp>";
    const FREE_MEM: &'static str = "free_mem<";
    const CPU_USAGE: &'static str = "cpu_usage>";
    const PROC_MEM: &'static str = "proc_mem";
    const SERVICE_KILLED: &'static str = "service_killed";
    const START: &'static str = "start";
    const BUS_ERROR: &'static str = "bus_error";
    const NO_DATA: &'static str = "no_data";
}

impl FromStr for NotificationKind {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        let replaced = s.replace(' ', "").to_lowercase();
        Ok(match () {
            _ if replaced.starts_with(Self::TEMP) => {
                Self::TempGt(parse_val(&replaced, Self::TEMP)?)
            }
            _ if replaced.starts_with(Self::FREE_MEM) => {
                Self::FreeMemLt(parse_val_f(&replaced, Self::FREE_MEM, parse_mem)?)
            }
            _ if replaced.starts_with(Self::CPU_USAGE) => {
                Self::CpuUsageGt(parse_val(&replaced, Self::CPU_USAGE)?)
            }
            _ if replaced.starts_with(Self::PROC_MEM) => {
                let split = replaced[Self::PROC_MEM.len()..]
                    .split('>')
                    .collect::<Vec<_>>();
                if split.len() != 2 {
                    return Err(());
                }
                let val = parse_mem(split[1])?;
                Self::ProcMemGt(split[0].to_string(), val)
            }
            _ if replaced == Self::SERVICE_KILLED => Self::ServiceKilled,
            _ if replaced == Self::START => Self::Start,
            _ if replaced == Self::BUS_ERROR => Self::BusError,
            _ if replaced == Self::NO_DATA => Self::NoData,
            _ => return Err(()),
        })
    }
}

impl Default for NotificationKind {
    fn default() -> Self {
        Self::NoData
    }
}

#[derive(Default, Debug)]
pub struct NotificationConfig {
    pub kind: NotificationKind,
    pub interval: f64,
}

impl Eq for NotificationConfig {}

impl PartialEq for NotificationConfig {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind && (self.interval - other.interval).abs() < F64_DELTA
    }
}

#[derive(Default, Debug)]
pub struct MainConfig {
    pub services: Vec<ServiceConfig>,
    pub io: Vec<IOConfig>,
    pub notificators: Vec<NotificatorConfig>,
    pub notifications: Vec<NotificationConfig>,
    pub core_sleep: f64,
    pub notificator_sleep: f64,
    pub io_sleep: f64,
    pub error_lifetime: f64,
    pub sys_temp_source: String,
}

impl Eq for MainConfig {}

impl PartialEq for MainConfig {
    fn eq(&self, other: &Self) -> bool {
        self.services == other.services
            && self.io == other.io
            && self.notificators == other.notificators
            && self.notifications == other.notifications
            && (self.core_sleep - other.core_sleep).abs() < F64_DELTA
            && (self.notificator_sleep - other.notificator_sleep).abs() < F64_DELTA
            && (self.io_sleep - other.io_sleep).abs() < F64_DELTA
            && (self.error_lifetime - other.error_lifetime).abs() < F64_DELTA
            && (self.sys_temp_source == other.sys_temp_source)
    }
}
