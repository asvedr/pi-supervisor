use crate::entities::errors::GetInfoError;

#[derive(Debug, Clone)]
pub struct SysInfo {
    pub temp: Result<f64, GetInfoError>,
    pub mem_total: Result<u64, GetInfoError>,
    pub mem_free: Result<u64, GetInfoError>,
    pub cpu_load: Result<f64, GetInfoError>,
}

impl Default for SysInfo {
    fn default() -> Self {
        Self {
            temp: Err(GetInfoError::NotAvailable),
            mem_total: Err(GetInfoError::NotAvailable),
            mem_free: Err(GetInfoError::NotAvailable),
            cpu_load: Err(GetInfoError::NotAvailable),
        }
    }
}

impl Eq for SysInfo {}

impl PartialEq for SysInfo {
    fn eq(&self, other: &Self) -> bool {
        fn cmp_res(a: &Result<f64, GetInfoError>, b: &Result<f64, GetInfoError>) -> bool {
            match (a, b) {
                (Ok(a), Ok(b)) => (a - b).abs() < 0.001,
                (Err(a), Err(b)) => a == b,
                _ => false,
            }
        }

        cmp_res(&self.temp, &other.temp)
            && cmp_res(&self.cpu_load, &other.cpu_load)
            && self.mem_total == other.mem_total
            && self.mem_free == other.mem_free
    }
}

#[derive(Debug, Clone)]
pub struct ProcExtInfo {
    pub mem_used: Result<u64, GetInfoError>,
    pub alive: Result<bool, GetInfoError>,
}

#[derive(Debug, Clone)]
pub struct ProcIntInfo {
    pub service: String,
    pub pid: u32,
    pub lifetime: u64,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ProcTotalInfo {
    pub mem_used: Result<u64, GetInfoError>,
    pub alive: Result<bool, GetInfoError>,
    pub service: String,
    pub pid: u32,
    pub lifetime: u64,
    pub start_required: bool,
    pub stop_required: bool,
    pub local_id: usize,
}

impl Default for ProcTotalInfo {
    fn default() -> Self {
        ProcTotalInfo {
            mem_used: Err(GetInfoError::NotAvailable),
            alive: Err(GetInfoError::NotAvailable),
            service: Default::default(),
            pid: Default::default(),
            lifetime: Default::default(),
            start_required: Default::default(),
            stop_required: Default::default(),
            local_id: Default::default(),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Status {
    pub sys_info: SysInfo,
    pub services: Vec<ProcTotalInfo>,
    pub killed: Vec<String>,
    pub terminate_all: bool,
}
