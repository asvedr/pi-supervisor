#[derive(Debug, Clone, PartialEq)]
pub enum CoreCommand {
    Start(String),
    Stop(String),
    StopSelf,
    Reboot,
}
