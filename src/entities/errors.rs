use std::io::Error;
use std::path::PathBuf;
use std::sync::PoisonError;
use std::time::SystemTime;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum GetInfoError {
    CantGet,
    CantParse,
    NotAvailable,
}

#[derive(Debug, Clone, PartialEq)]
pub enum StartStopError {
    NotFound,
    AlreadyRun,
    AlreadyStopped,
    CantSpawn(String),
    CantKill(String),
}

#[derive(Clone, Debug, PartialEq)]
pub enum CoreProcessErrorKind {
    StartStopError(String, StartStopError),
    CanNotReboot(String),
    InvalidService(String),
}

#[derive(Clone, Debug, PartialEq)]
pub enum TreeBuilderError {
    IO(String),
    InvalidProcEntry(String),
    CanNotOpenFile(PathBuf),
    InvalidPid(String),
    ParentPidNotFound,
    CanNotReadProcDir,
    LoopDetected,
}

#[derive(Clone, Debug, PartialEq)]
pub struct CoreProcessError {
    pub kind: CoreProcessErrorKind,
    pub when: SystemTime,
}

impl CoreProcessError {
    pub fn new(kind: CoreProcessErrorKind) -> Self {
        Self {
            kind,
            when: SystemTime::now(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum BusError {
    MutexPoisoned,
}

#[derive(Debug, PartialEq)]
pub enum CommandExecutorError {
    BusError(BusError),
    NoData,
    NotAvailable,
    ShellError(String),
}

#[derive(Debug)]
pub enum ParserError {
    InvalidCommand,
    ParamRequired,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ConfigParserError {
    CanNotReadFile,
    InvalidYaml,
    TopLevelNotDict,
    ObjectNotDict(&'static str),
    ObjectNotStr(&'static str),
    ObjectNotBool(&'static str),
    InvalidField(&'static str),
    FieldNotFound(&'static str),
}

#[derive(Debug, Clone, PartialEq)]
pub enum KillerError {
    IO(String),
    TreeBuilder(TreeBuilderError),
}

impl From<CoreProcessErrorKind> for CoreProcessError {
    fn from(src: CoreProcessErrorKind) -> Self {
        Self::new(src)
    }
}

impl<T> From<PoisonError<T>> for BusError {
    fn from(_: PoisonError<T>) -> Self {
        Self::MutexPoisoned
    }
}

impl From<BusError> for CommandExecutorError {
    fn from(src: BusError) -> Self {
        Self::BusError(src)
    }
}

impl ToString for CoreProcessError {
    fn to_string(&self) -> String {
        use crate::entities::errors::CoreProcessErrorKind::*;

        match &self.kind {
            StartStopError(ser, err) => {
                format!("Start/Stop service={}: {:?}", ser, err)
            }
            CanNotReboot(msg) => format!("CanNotReboot: {}", msg),
            InvalidService(name) => format!("Invalid service: {}", name),
        }
    }
}

impl ToString for CommandExecutorError {
    fn to_string(&self) -> String {
        match self {
            CommandExecutorError::BusError(err) => format!("Bus error: {:?}", err),
            CommandExecutorError::NoData => "No data".to_string(),
            CommandExecutorError::NotAvailable => "Not available".to_string(),
            CommandExecutorError::ShellError(err) => format!("Shell error: {}", err),
        }
    }
}

impl From<std::io::Error> for TreeBuilderError {
    fn from(value: Error) -> Self {
        Self::IO(value.to_string())
    }
}

impl From<TreeBuilderError> for KillerError {
    fn from(value: TreeBuilderError) -> Self {
        Self::TreeBuilder(value)
    }
}

impl From<std::io::Error> for KillerError {
    fn from(value: Error) -> Self {
        Self::IO(value.to_string())
    }
}
