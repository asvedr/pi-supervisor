use std::fmt::Debug;

#[derive(Debug, PartialEq, Eq)]
pub enum NotificationState {
    Ok,
    Alert,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Notification {
    pub name: String,
    pub state: NotificationState,
    pub text: String,
}

#[derive(Default)]
pub struct NotificationUpdate {
    pub ok: Vec<Notification>,
    pub alert: Vec<Notification>,
}

impl Notification {
    pub fn ok<S: ToString>(key: S) -> Self {
        Self {
            name: key.to_string(),
            state: NotificationState::Ok,
            text: "".to_string(),
        }
    }

    pub fn alert_err<S: ToString, E: Debug>(key: S, err: E) -> Self {
        Self {
            name: key.to_string(),
            state: NotificationState::Alert,
            text: format!("{:?}", err),
        }
    }

    pub fn alert_text<S1: ToString, S2: ToString>(key: S1, text: S2) -> Self {
        Self {
            name: key.to_string(),
            state: NotificationState::Alert,
            text: text.to_string(),
        }
    }
}

pub struct StdNames {}

impl StdNames {
    pub const START: &'static str = "start";
    pub const CPU: &'static str = "cpu";
    pub const FREE_MEM: &'static str = "free_mem";
    pub const TEMP: &'static str = "temp";
    pub const BUS_ERROR: &'static str = "bus_error";
    pub const NO_DATA: &'static str = "no_data";
    pub const SERVICE_KILLED: &'static str = "service_killed";
}
