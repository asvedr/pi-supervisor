pub enum IOCommand {
    Shell(String),
    StartService(String),
    StopService(String),
    StopSelf,
    Reboot,
    ShowSysInfo,
    ShowAliveServices,
    ShowAllServices,
    ShowErrors,
    Help,
}

#[derive(Default, Clone, Copy)]
pub struct Rights {
    pub shell: bool,
    pub loading: bool,
}
