use std::fs::File;
use std::io::Read;
use std::process::Command;
use std::str::FromStr;

use crate::entities::config::MainConfig;
use crate::entities::constants::{MEM_GB, MEM_KB, MEM_MB};
use crate::entities::errors::GetInfoError;
use crate::entities::info::{ProcExtInfo, SysInfo};
use crate::proto::core::IInfoCollector;

const SYS_MEM_SOURCE: &str = "/proc/meminfo";
const SYS_PROC_CMD_NAME: &str = "top";
const SYS_PROC_CMD_ARGS: &[&str] = &["-i", "-b", "-n", "1"];

pub struct SysInfoCollector {
    sys_temp_source: String,
}

fn read_file(path: &str) -> Result<String, GetInfoError> {
    let mut file = File::open(path).map_err(|_| GetInfoError::CantGet)?;
    let mut buf = String::new();
    file.read_to_string(&mut buf)
        .map_err(|_| GetInfoError::CantGet)?;
    Ok(buf)
}

fn read_parse_file<T, E, F: Fn(&str) -> Result<T, E>>(
    path: &str,
    parser: F,
) -> Result<T, GetInfoError> {
    let buf = read_file(path)?;
    parser(&buf).map_err(|_| GetInfoError::CantParse)
}

fn split_n_get(src: &str, sep: char, num: usize) -> Result<&str, ()> {
    let mut split = src.split(sep);
    for _ in 0..num {
        let _ = split.next();
    }
    match split.next() {
        None => Err(()),
        Some(token) => Ok(token),
    }
}

impl SysInfoCollector {
    pub fn new(config: &MainConfig) -> Self {
        Self {
            sys_temp_source: config.sys_temp_source.clone(),
        }
    }

    fn get_temp(&self) -> Result<f64, GetInfoError> {
        let path = &self.sys_temp_source;
        let temp = read_parse_file(path, |s| f64::from_str(s.trim()))?;
        Ok(temp / 1000.0)
    }

    pub fn get_row<'a>(src: &'a [&str], key: &str) -> Result<&'a str, GetInfoError> {
        for line in src {
            if line.starts_with(key) {
                return Ok(&line[key.len() + 1..]);
            }
        }
        Err(GetInfoError::CantParse)
    }

    pub fn get_mem_row_num(src: &[&str], key: &str) -> Result<u64, GetInfoError> {
        let row = Self::get_row(src, key)?.to_lowercase();
        let mut iter = row.split(' ').rev();
        let mult = match iter.next() {
            None => return Err(GetInfoError::CantParse),
            Some("b") => 1,
            Some("kb") => MEM_KB,
            Some("mb") => MEM_MB,
            Some("gb") => MEM_GB,
            Some(s) => panic!("unknown suffix: {}", s),
        };
        let data = match iter.next() {
            None => return Err(GetInfoError::CantParse),
            Some(val) => val,
        };
        let num = f64::from_str(data).map_err(|_| GetInfoError::CantParse)?;
        Ok(num as u64 * mult)
    }

    pub fn is_alive(src: &[&str]) -> Result<bool, GetInfoError> {
        let row = Self::get_row(src, "State")?.trim();
        let state = match row.chars().next() {
            None => return Err(GetInfoError::CantParse),
            Some(val) => val,
        };
        let alive = [
            'R', //  Running
            'S', //  Sleeping in an interruptible wait
            'D', //  Waiting in uninterruptible disk sleep
        ];
        let dead = [
            'Z', //  Zombie
            'T', //  Stopped (on a signal) or (before Linux 2.6.33) trace stopped
            't', //  Tracing stop (Linux 2.6.33 onward)
            'X', //  Dead (from Linux 2.6.0 onward)
        ];
        if alive.contains(&state) {
            return Ok(true);
        }
        if dead.contains(&state) {
            return Ok(false);
        }
        Err(GetInfoError::CantParse)
    }

    fn parse_mem(src: &str) -> Result<(u64, u64), GetInfoError> {
        let lines = src.split('\n').collect::<Vec<_>>();
        let total = Self::get_mem_row_num(&lines, "MemTotal")?;
        let free = Self::get_mem_row_num(&lines, "MemFree")?;
        Ok((total, free))
    }

    fn get_mem() -> Result<(u64, u64), GetInfoError> {
        read_parse_file(SYS_MEM_SOURCE, Self::parse_mem)
    }

    fn get_cpu() -> Result<f64, GetInfoError> {
        let info = Command::new(SYS_PROC_CMD_NAME)
            .args(SYS_PROC_CMD_ARGS)
            .output()
            .map_err(|_| GetInfoError::CantGet)?;
        let text = String::from_utf8(info.stdout).map_err(|_| GetInfoError::CantGet)?;
        let line = split_n_get(&text, '\n', 2).map_err(|_| GetInfoError::CantParse)?;
        if !line.starts_with("%Cpu(s):") {
            return Err(GetInfoError::CantParse);
        }
        let token = split_n_get(line, ':', 1).map_err(|_| GetInfoError::CantParse)?;
        let token = split_n_get(token, ',', 0)
            .map_err(|_| GetInfoError::CantParse)?
            .trim();
        let split = token.split(' ').collect::<Vec<_>>();
        if split.len() != 2 || split[1] != "us" {
            return Err(GetInfoError::CantParse);
        }
        f64::from_str(split[0]).map_err(|_| GetInfoError::CantParse)
    }
}

impl IInfoCollector for SysInfoCollector {
    fn get_sys_info(&self) -> SysInfo {
        let (mem_total, mem_free) = match Self::get_mem() {
            Ok((t, f)) => (Ok(t), Ok(f)),
            Err(err) => (Err(err.clone()), Err(err)),
        };
        SysInfo {
            temp: self.get_temp(),
            mem_total,
            mem_free,
            cpu_load: Self::get_cpu(),
        }
    }

    fn get_proc_info(&self, pid: u32) -> Option<ProcExtInfo> {
        let path = format!("/proc/{}/status", pid);
        let data = match read_file(&path) {
            Ok(val) => val,
            Err(_) => return None,
        };
        let lines = data.split('\n').collect::<Vec<_>>();
        let alive = Self::is_alive(&lines);
        Some(ProcExtInfo {
            mem_used: Self::get_mem_row_num(&lines, "VmRSS"),
            alive,
        })
    }
}
