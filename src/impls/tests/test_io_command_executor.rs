use crate::entities::command::CoreCommand;
use crate::entities::errors::{CommandExecutorError, CoreProcessError, CoreProcessErrorKind};
use crate::entities::info::{ProcTotalInfo, Status};
use crate::entities::io::{IOCommand, Rights};
use crate::impls::bus::Bus;
use crate::impls::core_buses::CoreBuses;
use crate::impls::io_command_executor::IOCommandExecutor;
use crate::proto::core::IBus;
use crate::proto::io::IIOCommandExecutor;

type Executor = IOCommandExecutor<CoreBuses<Bus<CoreCommand>, Bus<Status>, Bus<CoreProcessError>>>;

fn create_executor() -> Executor {
    Executor::new(CoreBuses {
        command: Bus::new(100),
        status: Bus::new(1),
        error: Bus::new(10),
    })
}

#[test]
fn test_exe_start_ser() {
    let exe = create_executor();
    let _ = exe.execute_command(
        IOCommand::StartService("aaa".to_string()),
        Rights::default(),
    );
    assert_eq!(
        exe.buses.command.pop().unwrap().unwrap(),
        CoreCommand::Start("aaa".to_string()),
    )
}

#[test]
fn test_exe_stop_ser() {
    let exe = create_executor();
    let _ = exe.execute_command(IOCommand::StopService("aaa".to_string()), Rights::default());
    assert_eq!(
        exe.buses.command.pop().unwrap().unwrap(),
        CoreCommand::Stop("aaa".to_string()),
    )
}

#[test]
fn test_exe_reboot() {
    let exe = create_executor();
    let _ = exe.execute_command(IOCommand::Reboot, Rights::default());
    assert_eq!(
        exe.buses.command.pop().unwrap().unwrap(),
        CoreCommand::Reboot,
    )
}

#[test]
fn test_exe_show_sys() {
    let exe = create_executor();
    let res = exe.execute_command(IOCommand::ShowSysInfo, Rights::default());
    assert_eq!(res, Err(CommandExecutorError::NoData));

    let _ = exe.buses.status.push(Status::default());
    let res = exe.execute_command(IOCommand::ShowSysInfo, Rights::default());
    assert_eq!(
        res.unwrap(),
        concat!(
            "temp: <NotAvailable>\n",
            "mem_total: <NotAvailable>\n",
            "mem_free: <NotAvailable>\n",
            "cpu_load: <NotAvailable>"
        )
    );
}

#[test]
fn test_exe_show_all_ser() {
    let exe = create_executor();
    let res = exe.execute_command(IOCommand::ShowAllServices, Rights::default());
    assert_eq!(res, Err(CommandExecutorError::NoData));

    let _ = exe.buses.status.push(Status::default());
    let res = exe.execute_command(IOCommand::ShowAllServices, Rights::default());
    assert_eq!(res.unwrap(), "<no services>");

    let _ = exe.buses.status.push(Status {
        services: vec![ProcTotalInfo {
            alive: Ok(true),
            service: "bep".to_string(),
            ..Default::default()
        }],
        ..Default::default()
    });
    let res = exe.execute_command(IOCommand::ShowAllServices, Rights::default());
    assert_eq!(res.unwrap(), "service: \"bep\", status: alive");
}

#[test]
fn test_exe_show_alive_ser() {
    let exe = create_executor();
    let res = exe.execute_command(IOCommand::ShowAliveServices, Rights::default());
    assert_eq!(res, Err(CommandExecutorError::NoData));

    let _ = exe.buses.status.push(Status {
        services: vec![ProcTotalInfo {
            alive: Ok(false),
            ..Default::default()
        }],
        ..Default::default()
    });
    let res = exe.execute_command(IOCommand::ShowAliveServices, Rights::default());
    assert_eq!(res.unwrap(), "<no alive services>");

    let _ = exe.buses.status.push(Status {
        services: vec![ProcTotalInfo {
            mem_used: Ok(10),
            alive: Ok(true),
            service: "bep".to_string(),
            pid: 0,
            lifetime: 0,
            start_required: false,
            stop_required: false,
            local_id: 0,
        }],
        ..Default::default()
    });
    let res = exe.execute_command(IOCommand::ShowAliveServices, Rights::default());
    assert_eq!(
        res.unwrap(),
        concat!(
            "service: bep\n",
            "    mem: 10 b\n",
            "    pid: 0\n",
            "    alive: 0",
        )
    );
}

#[test]
fn test_exe_show_errors() {
    let exe = create_executor();
    let res = exe.execute_command(IOCommand::ShowErrors, Rights::default());
    assert_eq!(res.unwrap(), "<no errors>");

    let kind = CoreProcessErrorKind::CanNotReboot("qwe".to_string());
    let err = CoreProcessError::new(kind);
    let _ = exe.buses.error.push(err);
    let res = exe.execute_command(IOCommand::ShowErrors, Rights::default());
    assert_eq!(res.unwrap(), "0.00 secs ago> CanNotReboot: qwe");
}

#[test]
fn test_exe_sh_ok() {
    let exe = create_executor();
    let res = exe.execute_command(
        IOCommand::Shell(r#"echo "hello""#.to_string()),
        Rights {
            shell: true,
            ..Default::default()
        },
    );
    assert_eq!(res.unwrap(), "<<stdout>>\nhello\n\n");
}

#[test]
fn test_exe_sh_not_allowed() {
    let exe = create_executor();
    let res = exe.execute_command(
        IOCommand::Shell(r#"echo "hello""#.to_string()),
        Rights::default(),
    );
    assert_eq!(res.unwrap_err(), CommandExecutorError::NotAvailable);
}

#[test]
fn test_exe_sh_error() {
    let exe = create_executor();
    let res = exe.execute_command(
        IOCommand::Shell("/a/b/c".to_string()),
        Rights {
            shell: true,
            ..Default::default()
        },
    );
    assert!(res.unwrap().starts_with("status: "));
}
