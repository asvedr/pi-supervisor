use crate::impls::bus::Bus;
use crate::proto::core::IBus;

fn create_bus(max_len: usize) -> Bus<&'static str> {
    Bus::new(max_len)
}

#[test]
fn test_empty_bus() {
    let bus = create_bus(10);
    assert_eq!(bus.pop().unwrap(), None);
    assert_eq!(bus.peek(3).unwrap(), Vec::<&'static str>::new());
}

#[test]
fn test_push_pop_peek_bus() {
    let bus = create_bus(10);
    bus.push("a").unwrap();
    bus.push("b").unwrap();
    bus.push("c").unwrap();

    assert_eq!(bus.peek(5).unwrap(), &["a", "b", "c"]);
    assert_eq!(bus.pop().unwrap(), Some("a"));
    assert_eq!(bus.peek(5).unwrap(), &["b", "c"]);
    bus.push("d").unwrap();
    assert_eq!(bus.peek(3).unwrap(), &["b", "c", "d"]);
    assert_eq!(bus.peek(2).unwrap(), &["b", "c"]);
    assert_eq!(bus.pop().unwrap(), Some("b"));
    assert_eq!(bus.pop().unwrap(), Some("c"));
    assert_eq!(bus.pop().unwrap(), Some("d"));
    assert_eq!(bus.pop().unwrap(), None);
}

#[test]
fn test_bus_overflow() {
    let bus = create_bus(2);
    bus.push("a").unwrap();
    bus.push("b").unwrap();
    assert_eq!(bus.peek(3).unwrap(), &["a", "b"]);
    bus.push("c").unwrap();
    assert_eq!(bus.peek(3).unwrap(), &["b", "c"]);
    assert_eq!(bus.pop().unwrap(), Some("b"));
    assert_eq!(bus.pop().unwrap(), Some("c"));
    assert_eq!(bus.pop().unwrap(), None);
}
