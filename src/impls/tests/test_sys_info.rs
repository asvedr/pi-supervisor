use crate::entities::config::MainConfig;
use crate::entities::constants::MEM_KB;
use crate::entities::errors::GetInfoError;
#[cfg(target_os = "linux")]
use crate::entities::info::SysInfo;
use crate::impls::sys_info_collector::SysInfoCollector;
use crate::proto::core::IInfoCollector;

#[cfg(target_os = "linux")]
#[test]
fn test_get_sys_info() {
    let collector = SysInfoCollector::new(&MainConfig::default());
    // do not panic
    let info = collector.get_sys_info();
    assert!(info.mem_free.is_ok());
    assert!(info.mem_total.is_ok());
    assert!(info.cpu_load.is_ok());
}

#[test]
fn test_parse_proc_info() {
    let src = r#"
Name:	pi_supervisor-b
Umask:	0022
State:	S (sleeping)
Tgid:	4396
Ngid:	0
Pid:	4396
PPid:	4237
TracerPid:	0
Uid:	0	0	0	0
Gid:	0	0	0	0
FDSize:	64
Groups:
NStgid:	4396
NSpid:	4396
NSpgid:	4237
NSsid:	1
VmPeak:	  136584 kB
VmSize:	   71048 kB
VmLck:	       0 kB
VmPin:	       0 kB
VmHWM:	     740 kB
VmRSS:	     740 kB
RssAnon:	     144 kB
RssFile:	     596 kB
RssShmem:	       0 kB
VmData:	    2424 kB
VmStk:	     132 kB
VmExe:	    1048 kB
VmLib:	    1696 kB
VmPTE:	      52 kB
VmSwap:	       0 kB
HugetlbPages:	       0 kB
CoreDumping:	0
THP_enabled:	1
Threads:	2
SigQ:	1/7502
SigPnd:	0000000000000000
ShdPnd:	0000000000000000
SigBlk:	0000000000000000
SigIgn:	0000000000001000
SigCgt:	0000000180000440
CapInh:	00000000a80425fb
CapPrm:	00000000a80425fb
CapEff:	00000000a80425fb
CapBnd:	00000000a80425fb
CapAmb:	0000000000000000
NoNewPrivs:	0
Seccomp:	2
Seccomp_filters:	1
Speculation_Store_Bypass:	vulnerable
Cpus_allowed:	f
Cpus_allowed_list:	0-3
Mems_allowed:	1
Mems_allowed_list:	0
voluntary_ctxt_switches:	48
nonvoluntary_ctxt_switches:	0
"#;
    let lines = src.split('\n').collect::<Vec<_>>();
    assert_eq!(SysInfoCollector::is_alive(&lines), Ok(true),);
    assert_eq!(
        SysInfoCollector::get_mem_row_num(&lines, "VmSize"),
        Ok(71048 * MEM_KB),
    );
}

#[cfg(target_os = "linux")]
#[test]
fn test_get_self_proc_info() {
    use std::process;

    let collector = SysInfoCollector::new(&MainConfig::default());
    let info = collector.get_proc_info(process::id()).unwrap();
    assert_eq!(info.alive, Ok(true))
}

#[test]
fn test_get_temp_ok() {
    let collector = SysInfoCollector::new(&MainConfig {
        sys_temp_source: "./test_data/temp".to_string(),
        ..Default::default()
    });
    let info = collector.get_sys_info();
    assert!((info.temp.unwrap() - 41.8).abs() < 0.1);
}

#[test]
fn test_get_temp_fail() {
    let collector = SysInfoCollector::new(&MainConfig {
        sys_temp_source: "./test_data/".to_string(),
        ..Default::default()
    });
    let info = collector.get_sys_info();
    assert_eq!(info.temp.unwrap_err(), GetInfoError::CantGet);
}
