use crate::entities::config::{
    IOConfig, KillReason, MainConfig, NotificationConfig, NotificationKind, NotificatorConfig,
    ServiceConfig, SocketConfig, TelegramConfig,
};
use crate::entities::constants::{
    DEFAULT_ERROR_LIFETIME, DEFAULT_SLEEP, DEFAULT_SYS_TEMP_SOURCE, MEM_GB, MEM_MB,
};
use crate::impls::config_parser::ConfigParser;
use crate::proto::config::IConfigParser;

const CONFIG_FULL: &str = r#"
services:
  - name: poet
    command: /usr/bin/poet --token 123
    start: false
    kill_on:
      - temp > 38.2
      - proc_mem > 10mb
      - free_mem < 1 gb
      - free_mem < 3
  - name: poker
    command: /usr/bin/poker -t 2
    env:
        robots: daniel,terence
        timeout: 2
io:
  - type: socket
    path: /tmp/val.sock
    timeout: 1
  - type: tg
    admin_id: 2
    token: abcd
    allow_shell: true
    allow_loading: true
notificators:
  - type: tg
    admin_id: 2
    notification_interval: 1
    token: abcd
notifications:
  - type: temp > 30
    interval: 3
  - type: service_killed
  - type: start
core_sleep: 1.1
notificator_sleep: 2
io_sleep: 3
error_lifetime: 4
killer: external
sys_temp_source: /a/b
"#;

const CONFIG_MINIMAL: &str = r#"
services:
  - name: poker
    command: /usr/bin/poker -t 2
io:
  - type: socket
    path: /tmp/val.sock
killer: internal
"#;

#[test]
fn test_parse_full() {
    let expected = MainConfig {
        services: vec![
            ServiceConfig {
                name: "poet".to_string(),
                command: "/usr/bin/poet --token 123".to_string(),
                start: false,
                kill_on: vec![
                    KillReason::TempGt(38.2),
                    KillReason::ProcMemGt(10 * MEM_MB),
                    KillReason::FreeMemLt(MEM_GB),
                    KillReason::FreeMemLt(3),
                ],
                vars: Vec::new(),
            },
            ServiceConfig {
                name: "poker".to_string(),
                command: "/usr/bin/poker -t 2".to_string(),
                start: true,
                kill_on: vec![],
                vars: vec![
                    ("robots".to_string(), "daniel,terence".to_string()),
                    ("timeout".to_string(), 2.to_string()),
                ],
            },
        ],
        io: vec![
            IOConfig::Socket(SocketConfig {
                path: "/tmp/val.sock".to_string(),
                timeout: 1.0,
            }),
            IOConfig::Tg(TelegramConfig {
                admin_id: 2,
                token: "abcd".to_string(),
                notification_interval: 0.0,
                allow_shell: true,
                allow_loading: true,
            }),
        ],
        notificators: vec![NotificatorConfig::Tg(TelegramConfig {
            admin_id: 2,
            token: "abcd".to_string(),
            notification_interval: 1.0,
            allow_shell: false,
            allow_loading: false,
        })],
        notifications: vec![
            NotificationConfig {
                kind: NotificationKind::TempGt(30.0),
                interval: 3.0,
            },
            NotificationConfig {
                kind: NotificationKind::ServiceKilled,
                interval: 0.0,
            },
            NotificationConfig {
                kind: NotificationKind::Start,
                interval: 0.0,
            },
        ],
        core_sleep: 1.1,
        notificator_sleep: 2.0,
        io_sleep: 3.0,
        error_lifetime: 4.0,
        sys_temp_source: "/a/b".to_string(),
    };
    assert_eq!(ConfigParser.parse_str(&CONFIG_FULL), Ok(expected))
}

#[test]
fn test_parse_minimal() {
    let expected = MainConfig {
        services: vec![ServiceConfig {
            name: "poker".to_string(),
            command: "/usr/bin/poker -t 2".to_string(),
            start: true,
            kill_on: vec![],
            vars: vec![],
        }],
        io: vec![IOConfig::Socket(SocketConfig {
            path: "/tmp/val.sock".to_string(),
            timeout: 1.0,
        })],
        notificators: vec![],
        notifications: vec![],
        core_sleep: DEFAULT_SLEEP,
        notificator_sleep: DEFAULT_SLEEP,
        io_sleep: DEFAULT_SLEEP,
        error_lifetime: DEFAULT_ERROR_LIFETIME,
        sys_temp_source: DEFAULT_SYS_TEMP_SOURCE.to_string(),
    };
    assert_eq!(ConfigParser.parse_str(CONFIG_MINIMAL), Ok(expected));
}
