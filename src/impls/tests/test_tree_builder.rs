use crate::entities::errors::TreeBuilderError;
use crate::impls::proc_tree_builder::ProcTreeBuilder;
use crate::proto::core::IProcTreeBuilder;

fn make_builder() -> ProcTreeBuilder {
    ProcTreeBuilder {
        target_dir: "./test_data/tree_builder_proc".into(),
    }
}

#[test]
fn test_get_children() {
    let builder = make_builder();
    assert_eq!(builder.get_all_children(123), Ok(vec![123, 1, 23, 3]));
    assert_eq!(builder.get_all_children(23), Ok(vec![23, 3]))
}

#[test]
fn test_no_such_process() {
    let builder = make_builder();
    assert_eq!(builder.get_all_children(777), Ok(vec![777]));
}

#[test]
fn test_ls_proc_error() {
    let mut builder = make_builder();
    builder.target_dir = "/a/b/c".into();
    assert_eq!(
        builder.get_all_children(123),
        Err(TreeBuilderError::CanNotReadProcDir)
    )
}

#[test]
fn test_loop_detected() {
    let builder = make_builder();
    assert_eq!(
        builder.get_all_children(999),
        Err(TreeBuilderError::LoopDetected)
    )
}
