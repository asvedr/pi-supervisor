use crate::entities::config::{KillReason, MainConfig, ServiceConfig};
use crate::entities::errors::GetInfoError;
use crate::entities::info::{ProcTotalInfo, Status, SysInfo};
use crate::impls::threshold_killer::ThresholdKiller;
use crate::proto::core::IThresholdKiller;

fn config() -> MainConfig {
    MainConfig {
        services: vec![
            ServiceConfig {
                name: "a".to_string(),
                command: "".to_string(),
                start: true,
                kill_on: vec![],
                vars: vec![],
            },
            ServiceConfig {
                name: "b".to_string(),
                command: "".to_string(),
                start: true,
                kill_on: vec![
                    KillReason::TempGt(40.0),
                    KillReason::FreeMemLt(10),
                    KillReason::ProcMemGt(20),
                ],
                vars: vec![],
            },
        ],
        ..Default::default()
    }
}

fn proc_info(name: &str, mem_used: Result<u64, GetInfoError>) -> ProcTotalInfo {
    ProcTotalInfo {
        mem_used,
        alive: Ok(true),
        service: name.to_string(),
        pid: 0,
        lifetime: 0,
        start_required: false,
        stop_required: false,
        local_id: 0,
    }
}

#[test]
fn test_no_kill_no_reasons() {
    let killer = ThresholdKiller::new(&config());
    let result = killer.find_services_to_kill(&Status {
        sys_info: SysInfo {
            temp: Err(GetInfoError::NotAvailable),
            mem_total: Err(GetInfoError::NotAvailable),
            mem_free: Err(GetInfoError::NotAvailable),
            cpu_load: Err(GetInfoError::NotAvailable),
        },
        services: vec![
            proc_info("a", Err(GetInfoError::NotAvailable)),
            proc_info("b", Err(GetInfoError::NotAvailable)),
        ],
        ..Default::default()
    });
    assert!(result.is_empty());
}

#[test]
fn test_no_kill_with_reasons() {
    let killer = ThresholdKiller::new(&config());
    let result = killer.find_services_to_kill(&Status {
        sys_info: SysInfo {
            temp: Ok(30.0),
            mem_total: Ok(1000),
            mem_free: Ok(900),
            cpu_load: Ok(1.0),
        },
        services: vec![proc_info("a", Ok(100)), proc_info("b", Ok(5))],
        ..Default::default()
    });
    assert!(result.is_empty());
}

#[test]
fn test_kill_by_proc_mem() {
    let killer = ThresholdKiller::new(&config());
    let result = killer.find_services_to_kill(&Status {
        sys_info: SysInfo {
            temp: Ok(30.0),
            mem_total: Ok(1000),
            mem_free: Ok(900),
            cpu_load: Ok(1.0),
        },
        services: vec![proc_info("a", Ok(100)), proc_info("b", Ok(100))],
        ..Default::default()
    });
    assert_eq!(result, &["b".to_string()]);
}

#[test]
fn test_kill_by_free_mem() {
    let killer = ThresholdKiller::new(&config());
    let result = killer.find_services_to_kill(&Status {
        sys_info: SysInfo {
            temp: Ok(30.0),
            mem_total: Ok(1000),
            mem_free: Ok(1),
            cpu_load: Ok(1.0),
        },
        services: vec![proc_info("a", Ok(100)), proc_info("b", Ok(5))],
        ..Default::default()
    });
    assert_eq!(result, &["b".to_string()]);
}

#[test]
fn test_kill_by_temp() {
    let killer = ThresholdKiller::new(&config());
    let result = killer.find_services_to_kill(&Status {
        sys_info: SysInfo {
            temp: Ok(100.0),
            mem_total: Ok(1000),
            mem_free: Ok(900),
            cpu_load: Ok(1.0),
        },
        services: vec![proc_info("a", Ok(100)), proc_info("b", Ok(1))],
        ..Default::default()
    });
    assert_eq!(result, &["b".to_string()]);
}
