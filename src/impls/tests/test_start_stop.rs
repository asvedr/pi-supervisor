use crate::entities::config::{MainConfig, ServiceConfig};
use crate::entities::errors::StartStopError;
use crate::impls::ext_killer::ExtKiller;
use crate::impls::proc_tree_builder::ProcTreeBuilder;
use crate::impls::start_stop::StartStop;
use crate::proto::core::IStartStop;

fn create_ss() -> StartStop<ExtKiller<ProcTreeBuilder>> {
    let ss = StartStop::new(
        Default::default(),
        &MainConfig {
            services: vec![
                ServiceConfig {
                    name: "ser".to_string(),
                    command: "sh".to_string(),
                    ..Default::default()
                },
                ServiceConfig {
                    name: "cmd_ok".to_string(),
                    command: "echo HI".to_string(),
                    ..Default::default()
                },
                ServiceConfig {
                    name: "cmd_fl".to_string(),
                    command: "false".to_string(),
                    ..Default::default()
                },
            ],
            ..Default::default()
        },
    );
    ss
}

#[test]
fn test_start_process_bad_name() {
    let mut ss = create_ss();
    assert_eq!(ss.start_service("abc"), Err(StartStopError::NotFound),)
}

#[test]
fn test_start_stop_process() {
    let mut ss = create_ss();
    ss.start_service("ser").unwrap();
    assert!(ss.get_pid("ser").is_some());
    ss.stop_service("ser").unwrap();
    assert!(ss.get_pid("ser").is_none());
}

#[test]
fn test_collect_info() {
    let mut ss = create_ss();
    assert!(ss.alive_services().is_empty());
    ss.start_service("ser").unwrap();
    let services = ss.alive_services();
    assert_eq!(services.len(), 1);
    assert_eq!(services[0].service, "ser");
    assert_eq!(services[0].lifetime, 0);
}
