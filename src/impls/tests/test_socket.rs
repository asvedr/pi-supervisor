use std::io::{Read, Write};
use std::os::unix::net::UnixStream;
use std::thread;
use std::time::Duration;

use crate::entities::config::{IOConfig, MainConfig, SocketConfig};
use crate::entities::errors::CommandExecutorError;
use crate::entities::io::{IOCommand, Rights};
use crate::impls::command_parser::CommandParser;
use crate::impls::socket_io_process::SocketIOProcess;
use crate::proto::core::IProcess;
use crate::proto::io::IIOCommandExecutor;
use crate::utils::system::listen_socket;

const SERVER_SOCKET: &str = "/tmp/pi_supervisor_server";
const CLIENT_SOCKET: &str = "/tmp/pi_supervisor_client";

#[derive(Default)]
struct MockExecutor {}

impl IIOCommandExecutor for MockExecutor {
    fn execute_command(&self, _: IOCommand, _: Rights) -> Result<String, CommandExecutorError> {
        Ok("rzhomba!".to_string())
    }
}

fn write_client(msg: String) {
    let mut server = UnixStream::connect(SERVER_SOCKET).unwrap();
    server.write(msg.as_bytes()).unwrap();
}

fn run_client() -> String {
    let listener = listen_socket(CLIENT_SOCKET).unwrap();
    let msg = format!("{}|start abc", CLIENT_SOCKET);
    write_client(msg);
    let mut inc = listener.incoming();
    let mut out = String::new();
    let _ = inc.next().unwrap().unwrap().read_to_string(&mut out);
    let msg = format!("{}|stop self", CLIENT_SOCKET);
    write_client(msg);
    return out;
}

#[test]
fn test_socket_wf() {
    let s_conf = SocketConfig {
        path: SERVER_SOCKET.to_string(),
        timeout: 0.1,
    };
    let mut proc = SocketIOProcess::new(
        CommandParser::default(),
        MockExecutor::default(),
        &MainConfig {
            io: vec![IOConfig::Socket(s_conf)],
            io_sleep: 0.1,
            ..Default::default()
        },
    )
    .unwrap();
    let _handle = thread::spawn(move || proc.run());
    thread::sleep(Duration::from_secs_f64(1.5));
    let resp = run_client();
    assert_eq!(resp, "ok|rzhomba!");
}
