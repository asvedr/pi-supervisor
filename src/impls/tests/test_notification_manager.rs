use std::thread;
use std::time::Duration;

use crate::entities::config::{MainConfig, NotificationConfig, NotificationKind};
use crate::entities::errors::{BusError, GetInfoError};
use crate::entities::info::{ProcTotalInfo, Status, SysInfo};
use crate::entities::notifications::{Notification, StdNames};
use crate::impls::notification_handlers;
use crate::impls::notification_manager::NotificationManager;
use crate::proto::notifications::INotificationManager;

fn create_manager(config: MainConfig) -> NotificationManager {
    NotificationManager::new(notification_handlers::all(), &config)
}

#[test]
fn test_start_notification() {
    let config = MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::Start,
            ..Default::default()
        }],
        ..Default::default()
    };
    let mut manager = create_manager(config);
    let upd = manager.process_status(&Status::default());
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok(StdNames::START)]);
    let upd = manager.process_status(&Status::default());
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok(StdNames::START)]);
    manager.post_send_action(&Notification::ok(StdNames::START));
    let upd = manager.process_status(&Status::default());
    assert!(upd.alert.is_empty());
    assert!(upd.ok.is_empty());
}

#[test]
fn test_notification_temp() {
    let config = MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::TempGt(30.0),
            interval: 10.0,
        }],
        ..Default::default()
    };
    let mut manager = create_manager(config);
    let mut status = Status {
        sys_info: SysInfo {
            temp: Err(GetInfoError::NotAvailable),
            ..Default::default()
        },
        ..Default::default()
    };
    let upd = manager.process_status(&status);
    assert_eq!(upd.ok.len() + upd.alert.len(), 0);
    status.sys_info.temp = Ok(40.0);
    let upd = manager.process_status(&status);
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::TEMP,
            "temperature 40.00 > 30.00"
        ),]
    );
    status.sys_info.temp = Ok(25.0);
    let upd = manager.process_status(&status);
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok(StdNames::TEMP)]);
}

#[test]
fn test_notification_mem() {
    let config = MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::FreeMemLt(100),
            interval: 10.0,
        }],
        ..Default::default()
    };
    let mut manager = create_manager(config);
    let mut status = Status {
        sys_info: SysInfo {
            mem_free: Err(GetInfoError::NotAvailable),
            ..Default::default()
        },
        ..Default::default()
    };
    let upd = manager.process_status(&status);
    assert_eq!(upd.ok.len() + upd.alert.len(), 0);
    status.sys_info.mem_free = Ok(40);
    let upd = manager.process_status(&status);
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::FREE_MEM,
            "free mem 40 < 100"
        ),]
    );
    status.sys_info.mem_free = Ok(101);
    let upd = manager.process_status(&status);
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok(StdNames::FREE_MEM)]);
}

#[test]
fn test_notification_cpu() {
    let config = MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::CpuUsageGt(1.0),
            interval: 10.0,
        }],
        ..Default::default()
    };
    let mut manager = create_manager(config);
    let mut status = Status {
        sys_info: SysInfo {
            cpu_load: Err(GetInfoError::NotAvailable),
            ..Default::default()
        },
        ..Default::default()
    };
    let upd = manager.process_status(&status);
    assert_eq!(upd.ok.len() + upd.alert.len(), 0);
    status.sys_info.cpu_load = Ok(1.5);
    let upd = manager.process_status(&status);
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::CPU,
            "cpu usage 1.50 > 1.00"
        ),]
    );
    status.sys_info.cpu_load = Ok(0.8);
    let upd = manager.process_status(&status);
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok(StdNames::CPU)]);
}

#[test]
fn test_notification_proc_mem() {
    let config = MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::ProcMemGt("a".to_string(), 50),
            interval: 10.0,
        }],
        ..Default::default()
    };
    let mut manager = create_manager(config);
    let mut status = Status {
        sys_info: SysInfo::default(),
        services: vec![ProcTotalInfo {
            mem_used: Ok(60),
            service: "abc".to_string(),
            ..Default::default()
        }],
        ..Default::default()
    };
    let upd = manager.process_status(&status);
    assert_eq!(upd.ok.len() + upd.alert.len(), 0);
    status.services[0].mem_used = Err(GetInfoError::NotAvailable);
    status.services[0].service = "a".to_string();
    let upd = manager.process_status(&status);
    assert_eq!(upd.ok.len() + upd.alert.len(), 0);
    status.services[0].mem_used = Ok(60);
    let upd = manager.process_status(&status);
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            "SERVICE_a_MEM",
            "service \"a\" is consuming mem 60 > 50",
        ),]
    );
    status.services[0].mem_used = Ok(40);
    let upd = manager.process_status(&status);
    assert!(upd.alert.is_empty());
    assert_eq!(upd.ok, &[Notification::ok("SERVICE_a_MEM")]);
}

#[test]
fn test_notification_repeat_alert() {
    let mut manager = create_manager(MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::NoData,
            interval: 0.5,
        }],
        ..Default::default()
    });
    let upd = manager.process_no_data();
    assert!(upd.ok.is_empty());
    assert_eq!(upd.alert.len(), 1);
    manager.post_send_action(&upd.alert[0]);
    let upd = manager.process_no_data();
    assert!(upd.ok.is_empty());
    assert!(upd.alert.is_empty());
    thread::sleep(Duration::from_secs_f64(0.6));
    let upd = manager.process_no_data();
    assert!(upd.ok.is_empty());
    assert_eq!(upd.alert.len(), 1);
}

#[test]
fn test_notification_no_data() {
    let mut manager = create_manager(MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::NoData,
            interval: 10.0,
        }],
        ..Default::default()
    });
    let upd = manager.process_no_data();
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::NO_DATA,
            "no data from core"
        )]
    );
}

#[test]
fn test_notification_service_killed() {
    let mut manager = create_manager(MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::ServiceKilled,
            interval: 10.0,
        }],
        ..Default::default()
    });
    let upd = manager.process_status(&Status {
        killed: vec!["a".to_string()],
        ..Default::default()
    });
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::SERVICE_KILLED,
            "killed: [\"a\"]"
        )]
    );
}

#[test]
fn test_notification_bus_error() {
    let mut manager = create_manager(MainConfig {
        notifications: vec![NotificationConfig {
            kind: NotificationKind::BusError,
            interval: 10.0,
        }],
        ..Default::default()
    });
    let upd = manager.process_bus_error(BusError::MutexPoisoned);
    assert!(upd.ok.is_empty());
    assert_eq!(
        upd.alert,
        &[Notification::alert_text(
            StdNames::BUS_ERROR,
            "MutexPoisoned"
        )]
    );
}
