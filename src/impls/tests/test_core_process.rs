use std::collections::HashMap;

use crate::entities::command::CoreCommand;
use crate::entities::config::{MainConfig, ServiceConfig};
use crate::entities::errors::{
    CoreProcessError, CoreProcessErrorKind, GetInfoError, StartStopError,
};
use crate::entities::info::{ProcExtInfo, ProcIntInfo, Status, SysInfo};
use crate::impls::bus::Bus;
use crate::impls::core_buses::CoreBuses;
use crate::impls::core_process::CoreProcess;
use crate::proto::core::{IBus, IInfoCollector, IProcess, IStartStop, IThresholdKiller};

struct MockCollector {
    info: SysInfo,
    proc_info: HashMap<u32, ProcExtInfo>,
}

#[derive(Default)]
struct MockStartStop {
    started: Vec<String>,
    stopped: Vec<String>,
    restarted: Vec<String>,
    alive: Vec<ProcIntInfo>,
    start_error: Option<StartStopError>,
    stop_error: Option<StartStopError>,
    restart_error: Option<StartStopError>,
}

#[derive(Default)]
struct MockKiller {
    services_to_kill: Vec<String>,
}

impl IThresholdKiller for MockKiller {
    fn find_services_to_kill(&self, _: &Status) -> Vec<String> {
        self.services_to_kill.clone()
    }
}

impl IInfoCollector for MockCollector {
    fn get_sys_info(&self) -> SysInfo {
        self.info.clone()
    }

    fn get_proc_info(&self, pid: u32) -> Option<ProcExtInfo> {
        self.proc_info.get(&pid).cloned()
    }
}

impl IStartStop for MockStartStop {
    fn start_service(&mut self, key: &str) -> Result<(), StartStopError> {
        self.started.push(key.to_string());
        self.start_error.clone().map_or(Ok(()), Err)
    }

    fn stop_service(&mut self, key: &str) -> Result<(), StartStopError> {
        self.stopped.push(key.to_string());
        self.stop_error.clone().map_or(Ok(()), Err)
    }

    fn restart_service(&mut self, key: &str) -> Result<(), StartStopError> {
        self.restarted.push(key.to_string());
        self.restart_error.clone().map_or(Ok(()), Err)
    }

    fn get_pid(&self, _: &str) -> Option<u32> {
        todo!()
    }

    fn alive_services(&mut self) -> Vec<ProcIntInfo> {
        self.alive.clone()
    }
}

type TestBuses = CoreBuses<Bus<CoreCommand>, Bus<Status>, Bus<CoreProcessError>>;
type TestCoreProcess = CoreProcess<MockCollector, MockStartStop, TestBuses, MockKiller>;

fn create_core(config: &MainConfig) -> TestCoreProcess {
    TestCoreProcess::new(
        MockCollector {
            info: SysInfo {
                temp: Err(GetInfoError::NotAvailable),
                mem_total: Err(GetInfoError::NotAvailable),
                mem_free: Err(GetInfoError::NotAvailable),
                cpu_load: Err(GetInfoError::NotAvailable),
            },
            proc_info: Default::default(),
        },
        MockStartStop::default(),
        CoreBuses {
            command: Bus::new(100),
            status: Bus::new(1),
            error: Bus::new(10),
        },
        MockKiller::default(),
        config,
    )
}

fn default_config() -> MainConfig {
    MainConfig {
        services: vec![
            ServiceConfig {
                name: "aaa".to_string(),
                command: "sh a".to_string(),
                start: false,
                kill_on: vec![],
                vars: vec![],
            },
            ServiceConfig {
                name: "xyz".to_string(),
                command: "2 + 3".to_string(),
                start: false,
                kill_on: vec![],
                vars: vec![],
            },
        ],
        core_sleep: 0.01,
        ..Default::default()
    }
}

#[test]
fn test_stop_by_signal() {
    let mut core = create_core(&default_config());
    assert_eq!(core.iteration(), true);
    let _ = core.buses.command.push(CoreCommand::StopSelf);
    assert_eq!(core.iteration(), false);

    assert!(core.buses.command.pop().unwrap().is_none());
    assert!(core.buses.error.pop().unwrap().is_none());
    let status = core.buses.status.pop().unwrap().unwrap();
    assert_eq!(
        status,
        Status {
            sys_info: SysInfo {
                temp: Err(GetInfoError::NotAvailable),
                mem_total: Err(GetInfoError::NotAvailable),
                mem_free: Err(GetInfoError::NotAvailable),
                cpu_load: Err(GetInfoError::NotAvailable)
            },
            terminate_all: true,
            ..Default::default()
        }
    )
}

fn xyz_autostart_config() -> MainConfig {
    let mut config = default_config();
    config.services[1].start = true;
    config
}

#[test]
fn test_start_service_cmd_ok() {
    let mut core = create_core(&default_config());
    core.iteration();
    assert!(core.start_stop.started.is_empty());
    core.buses
        .command
        .push(CoreCommand::Start("aaa".to_string()))
        .unwrap();
    core.iteration();
    assert_eq!(core.start_stop.restarted, &["aaa".to_string()]);
    assert_eq!(core.buses.error.pop().unwrap(), None);
}

#[test]
fn test_start_service_failed() {
    let mut core = create_core(&default_config());
    core.iteration();
    let ss_error = StartStopError::CantSpawn("".to_string());
    core.start_stop.restart_error = Some(ss_error.clone());
    core.buses
        .command
        .push(CoreCommand::Start("aaa".to_string()))
        .unwrap();
    core.iteration();
    assert_eq!(core.start_stop.restarted, &["aaa".to_string()]);
    assert_eq!(
        core.buses.error.pop().unwrap().unwrap().kind,
        CoreProcessErrorKind::StartStopError("aaa".to_string(), ss_error),
    );
}

#[test]
fn test_start_service_wrong_name() {
    let mut core = create_core(&default_config());
    core.iteration();
    core.buses
        .command
        .push(CoreCommand::Start("bbb".to_string()))
        .unwrap();
    core.iteration();
    assert!(core.start_stop.restarted.is_empty());
    assert_eq!(
        core.buses.error.pop().unwrap().unwrap().kind,
        CoreProcessErrorKind::InvalidService("bbb".to_string()),
    );
}

#[test]
fn test_stop_service_auto_ok() {
    let mut core = create_core(&default_config());
    core.start_stop.alive = vec![ProcIntInfo {
        service: "aaa".to_string(),
        pid: 123,
        lifetime: 1,
    }];
    core.iteration();
    assert_eq!(core.start_stop.stopped, &["aaa".to_string()]);
}

#[test]
fn test_stop_service_cmd_ok() {
    let mut core = create_core(&xyz_autostart_config());
    core.start_stop.alive = vec![ProcIntInfo {
        service: "xyz".to_string(),
        pid: 123,
        lifetime: 1,
    }];
    core.iteration();
    assert!(core.start_stop.stopped.is_empty());
    let _ = core
        .buses
        .command
        .push(CoreCommand::Stop("xyz".to_string()));
    core.iteration();
    assert_eq!(core.start_stop.stopped, &["xyz".to_string()]);
}

#[test]
fn test_stop_service_failed() {
    let mut core = create_core(&default_config());
    core.start_stop.alive = vec![ProcIntInfo {
        service: "aaa".to_string(),
        pid: 123,
        lifetime: 1,
    }];
    core.start_stop.stop_error = Some(StartStopError::AlreadyStopped);
    core.iteration();
    assert_eq!(core.start_stop.stopped, &["aaa".to_string()]);
    assert_eq!(
        core.buses.error.pop().unwrap().unwrap().kind,
        CoreProcessErrorKind::StartStopError("aaa".to_string(), StartStopError::AlreadyStopped),
    )
}

#[test]
fn test_stop_service_wrong_name() {
    let mut core = create_core(&xyz_autostart_config());
    core.start_stop.alive = vec![ProcIntInfo {
        service: "xyz".to_string(),
        pid: 123,
        lifetime: 1,
    }];
    let _ = core
        .buses
        .command
        .push(CoreCommand::Stop("bbb".to_string()));
    core.iteration();
    assert!(core.start_stop.stopped.is_empty());
    assert_eq!(
        core.buses.error.pop().unwrap().unwrap().kind,
        CoreProcessErrorKind::InvalidService("bbb".to_string()),
    )
}

#[test]
fn test_auto_restart_stopped_service() {
    let mut core = create_core(&xyz_autostart_config());
    core.start_stop.alive = vec![];
    core.iteration();
    assert_eq!(core.start_stop.restarted, &["xyz".to_string()],);
    core.start_stop.restarted.clear();
    core.start_stop.alive = vec![ProcIntInfo {
        service: "xyz".to_string(),
        pid: 1,
        lifetime: 0,
    }];
    core.iteration();
    assert!(core.start_stop.started.is_empty());
    core.start_stop.alive = vec![];
    core.iteration();
    assert_eq!(core.start_stop.restarted, &["xyz".to_string()],);
}

#[test]
fn test_reboot() {
    let mut core = create_core(&default_config());
    let _ = core.buses.command.push(CoreCommand::Reboot);
    core.iteration();
    let err = core.buses.error.pop().unwrap().unwrap();
    assert!(matches!(err.kind, CoreProcessErrorKind::CanNotReboot(_)));
}

#[test]
fn test_kill_service() {
    let mut core = create_core(&xyz_autostart_config());
    core.start_stop.alive = vec![ProcIntInfo {
        service: "xyz".to_string(),
        pid: 1,
        lifetime: 0,
    }];
    core.iteration();
    assert!(core.start_stop.stopped.is_empty());
    let status = core.buses.status.pop().unwrap().unwrap();
    assert!(status.killed.is_empty());
    core.killer.services_to_kill.push("xyz".to_string());
    core.iteration();
    assert_eq!(core.start_stop.stopped, &["xyz".to_string()]);
    let status = core.buses.status.pop().unwrap().unwrap();
    assert_eq!(status.killed, &["xyz".to_string()]);
}

#[test]
fn test_prevent_service_starting() {
    let mut core = create_core(&xyz_autostart_config());
    core.killer.services_to_kill.push("xyz".to_string());
    core.iteration();
    assert!(core.start_stop.restarted.is_empty());
    assert!(core.start_stop.started.is_empty());
    let status = core.buses.status.pop().unwrap().unwrap();
    assert_eq!(status.killed, &["xyz".to_string()]);
}
