use std::time::Duration;

use crate::entities::config::{NotificationConfig, NotificationKind};
use crate::entities::info::{ProcTotalInfo, Status};
use crate::entities::notifications::Notification;
use crate::proto::notifications::{INotificationHandler, INotificationHandlerFactory};

pub struct Handler {
    proc: String,
    label: String,
    limit: u64,
    interval: Duration,
}

#[derive(Default)]
pub struct Factory {}

impl Handler {
    fn find_proc_status<'a>(&self, status: &'a Status) -> Option<&'a ProcTotalInfo> {
        status
            .services
            .iter()
            .find(|&service| service.service == self.proc)
    }
}

impl INotificationHandler for Handler {
    fn process_status(&self, status: &Status) -> Option<(Notification, Duration)> {
        let proc = self.find_proc_status(status)?;
        let val = match proc.mem_used {
            Ok(val) if val <= self.limit => return None,
            Err(_) => return None,
            Ok(val) => val,
        };
        let notif = Notification::alert_text(
            &self.label,
            format!(
                "service {:?} is consuming mem {} > {}",
                self.proc, val, self.limit
            ),
        );
        Some((notif, self.interval))
    }
}

impl INotificationHandlerFactory for Factory {
    fn produce(&self, conf: &NotificationConfig) -> Option<Box<dyn INotificationHandler>> {
        let (proc, limit) = match conf.kind {
            NotificationKind::ProcMemGt(ref proc, mem) => (proc.clone(), mem),
            _ => return None,
        };
        let label = format!("SERVICE_{}_MEM", proc);
        let handler = Handler {
            proc,
            limit,
            label,
            interval: Duration::from_secs_f64(conf.interval),
        };
        Some(Box::new(handler))
    }
}
