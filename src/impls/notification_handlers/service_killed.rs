use std::time::Duration;

use crate::entities::config::{NotificationConfig, NotificationKind};
use crate::entities::info::Status;
use crate::entities::notifications::{Notification, StdNames};
use crate::proto::notifications::{INotificationHandler, INotificationHandlerFactory};

pub struct Handler {
    interval: Duration,
}

#[derive(Default)]
pub struct Factory {}

impl INotificationHandler for Handler {
    fn process_status(&self, status: &Status) -> Option<(Notification, Duration)> {
        if status.killed.is_empty() {
            return None;
        }
        let notif = Notification::alert_text(
            StdNames::SERVICE_KILLED,
            format!("killed: {:?}", status.killed),
        );
        Some((notif, self.interval))
    }
}

impl INotificationHandlerFactory for Factory {
    fn produce(&self, conf: &NotificationConfig) -> Option<Box<dyn INotificationHandler>> {
        if !matches!(conf.kind, NotificationKind::ServiceKilled) {
            return None;
        }
        let handler = Handler {
            interval: Duration::from_secs_f64(conf.interval),
        };
        Some(Box::new(handler))
    }
}
