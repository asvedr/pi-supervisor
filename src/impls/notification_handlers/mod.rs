use crate::proto::notifications::INotificationHandlerFactory;

mod cpu_usage;
mod free_mem;
mod proc_mem;
mod service_killed;
mod temp;

#[allow(clippy::box_default)]
pub fn all() -> Vec<Box<dyn INotificationHandlerFactory>> {
    vec![
        Box::new(temp::Factory::default()),
        Box::new(free_mem::Factory::default()),
        Box::new(cpu_usage::Factory::default()),
        Box::new(proc_mem::Factory::default()),
        Box::new(service_killed::Factory::default()),
    ]
}
