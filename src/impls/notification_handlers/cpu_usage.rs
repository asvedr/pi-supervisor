use crate::entities::config::{NotificationConfig, NotificationKind};
use crate::entities::info::Status;
use crate::entities::notifications::{Notification, StdNames};
use crate::proto::notifications::{INotificationHandler, INotificationHandlerFactory};
use std::time::Duration;

pub struct Handler {
    limit: f64,
    interval: Duration,
}

#[derive(Default)]
pub struct Factory {}

impl INotificationHandler for Handler {
    fn process_status(&self, status: &Status) -> Option<(Notification, Duration)> {
        let val = match status.sys_info.cpu_load {
            Ok(val) if val <= self.limit => return None,
            Err(_) => return None,
            Ok(val) => val,
        };
        let notif = Notification::alert_text(
            StdNames::CPU,
            format!("cpu usage {:.2} > {:.2}", val, self.limit),
        );
        Some((notif, self.interval))
    }
}

impl INotificationHandlerFactory for Factory {
    fn produce(&self, conf: &NotificationConfig) -> Option<Box<dyn INotificationHandler>> {
        let limit = match conf.kind {
            NotificationKind::CpuUsageGt(val) => val,
            _ => return None,
        };
        let handler = Handler {
            limit,
            interval: Duration::from_secs_f64(conf.interval),
        };
        Some(Box::new(handler))
    }
}
