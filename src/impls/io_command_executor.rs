use std::time::SystemTime;

use crate::entities::command::CoreCommand;
use crate::entities::constants::MEM_KB;
use crate::entities::errors::{CommandExecutorError, GetInfoError};
use crate::entities::info::Status;
use crate::entities::io::{IOCommand, Rights};
use crate::proto::core::ICoreBuses;
use crate::proto::io::IIOCommandExecutor;
use crate::utils::system::execute_shell;

pub struct IOCommandExecutor<B: ICoreBuses> {
    pub buses: B,
}

fn show_field<T, F: Fn(&T) -> String>(
    label: &str,
    field: &Result<T, GetInfoError>,
    to_str: F,
) -> String {
    let value = match field {
        Ok(x) => to_str(x),
        Err(err) => format!("<{:?}>", err),
    };
    format!("{}: {}", label, value)
}

fn show_alive(val: &bool) -> String {
    (if *val { "alive" } else { "dead" }).to_string()
}

fn show_mem(src: &u64) -> String {
    let mut val = *src;
    const PREFIXES: &[&str] = &["b", "kb", "mb"];
    for pref in PREFIXES {
        if val < MEM_KB {
            return format!("{} {}", val, pref);
        }
        val /= MEM_KB;
    }
    format!("{} gb", val)
}

impl<B: ICoreBuses> IOCommandExecutor<B> {
    pub fn new(buses: B) -> Self {
        Self { buses }
    }

    fn send_command(&self, cmd: CoreCommand) -> Result<String, CommandExecutorError> {
        self.buses.command().push(cmd)?;
        Ok("sent".to_string())
    }

    fn get_status(&self) -> Result<Status, CommandExecutorError> {
        let mut values = self.buses.status().peek(1)?;
        if values.len() == 1 {
            Ok(values.remove(0))
        } else {
            Err(CommandExecutorError::NoData)
        }
    }

    fn show_sys_info(&self) -> Result<String, CommandExecutorError> {
        let status = self.get_status()?;
        let info = status.sys_info;
        let report = [
            show_field("temp", &info.temp, |val| format!("{:.2}", val)),
            show_field("mem_total", &info.mem_total, show_mem),
            show_field("mem_free", &info.mem_free, show_mem),
            show_field("cpu_load", &info.cpu_load, |val| format!("{:.2}", val)),
        ]
        .join("\n");
        Ok(report)
    }

    fn show_alive_services(&self) -> Result<String, CommandExecutorError> {
        let status = self.get_status()?;
        let mut results = Vec::new();
        for service in status.services {
            if service.alive != Ok(true) {
                continue;
            }
            let msg = format!(
                "service: {ser}\n    {mem}\n    pid: {pid}\n    alive: {lt}",
                ser = service.service,
                mem = show_field("mem", &service.mem_used, show_mem),
                pid = service.pid,
                lt = service.lifetime
            );
            results.push(msg);
        }
        if results.is_empty() {
            return Ok("<no alive services>".to_string());
        }
        Ok(results.join("\n"))
    }

    fn show_all_services(&self) -> Result<String, CommandExecutorError> {
        let status = self.get_status()?;
        if status.services.is_empty() {
            return Ok("<no services>".to_string());
        }
        let msg = status
            .services
            .into_iter()
            .map(|service| {
                format!(
                    "service: {:?}, {}",
                    service.service,
                    show_field("status", &service.alive, show_alive),
                )
            })
            .collect::<Vec<_>>()
            .join("\n");
        Ok(msg)
    }

    fn show_errors(&self) -> Result<String, CommandExecutorError> {
        let bus = self.buses.error();
        let errors = bus.peek(bus.max_len())?;
        if errors.is_empty() {
            return Ok("<no errors>".to_string());
        }
        let now = SystemTime::now();
        let msg = errors
            .into_iter()
            .map(|err| {
                let when = now.duration_since(err.when).unwrap();
                let when = when.as_secs_f64();
                format!("{:.2} secs ago> {}", when, err.to_string())
            })
            .collect::<Vec<_>>()
            .join("\n");
        Ok(msg)
    }

    fn execute_shell(cmd: &str) -> Result<String, CommandExecutorError> {
        let (out, err, status) = execute_shell(cmd)
            .map_err(|err| CommandExecutorError::ShellError(format!("{:?}", err)))?;
        let mut report = String::new();
        if !status.success() {
            if let Some(code) = status.code() {
                report = format!("status: {:?}\n", code);
            }
        }
        if !out.is_empty() {
            report.push_str(&format!("<<stdout>>\n{}\n", out));
        }
        if !err.is_empty() {
            report.push_str(&format!("<<stderr>>\n{}\n", err));
        }
        Ok(if report.is_empty() {
            "status: ok".to_string()
        } else {
            report
        })
    }
}

impl<B: ICoreBuses> IIOCommandExecutor for IOCommandExecutor<B> {
    fn execute_command(
        &self,
        command: IOCommand,
        rights: Rights,
    ) -> Result<String, CommandExecutorError> {
        match command {
            IOCommand::Shell(sh_cmd) if rights.shell => Self::execute_shell(&sh_cmd),
            IOCommand::Shell(_) => Err(CommandExecutorError::NotAvailable),
            IOCommand::StartService(name) => self.send_command(CoreCommand::Start(name)),
            IOCommand::StopService(name) => self.send_command(CoreCommand::Stop(name)),
            IOCommand::StopSelf => self.send_command(CoreCommand::StopSelf),
            IOCommand::Reboot => self.send_command(CoreCommand::Reboot),
            IOCommand::ShowSysInfo => self.show_sys_info(),
            IOCommand::ShowAliveServices => self.show_alive_services(),
            IOCommand::ShowAllServices => self.show_all_services(),
            IOCommand::ShowErrors => self.show_errors(),
            IOCommand::Help => unreachable!(),
        }
    }
}
