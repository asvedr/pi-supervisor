use std::process::{Child, Command};

use crate::entities::constants::SHELL;
use crate::entities::errors::{KillerError, TreeBuilderError};
use crate::proto::core::{IKiller, IProcTreeBuilder};

#[derive(Default)]
pub struct ExtKiller<TB> {
    tree_builder: TB,
}

impl<TB: IProcTreeBuilder> IKiller for ExtKiller<TB> {
    fn name(&self) -> &str {
        "external"
    }

    fn kill(&self, process: &mut Child) -> Result<(), KillerError> {
        let id = process.id();
        let tree = match self.tree_builder.get_all_children(id) {
            Ok(val) => val,
            Err(TreeBuilderError::CanNotReadProcDir) => {
                eprintln!("Can not read proc dir. Children ignored");
                vec![id]
            }
            Err(err) => return Err(err.into()),
        };
        let mut cmd = "kill -9".to_string();
        for pid in tree {
            cmd.push(' ');
            cmd.push_str(&pid.to_string());
        }
        let mut handler = Command::new(SHELL).arg("-c").arg(cmd).spawn()?;
        handler.wait()?;
        process.wait()?;
        Ok(())
    }
}
