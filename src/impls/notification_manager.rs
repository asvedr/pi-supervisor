use std::collections::{HashMap, HashSet};
use std::time::{Duration, SystemTime};

use crate::entities::config::{MainConfig, NotificationConfig, NotificationKind};
use crate::entities::errors::BusError;
use crate::entities::info::Status;
use crate::entities::notifications::{
    Notification, NotificationState, NotificationUpdate, StdNames,
};
use crate::proto::notifications::{
    INotificationHandler, INotificationHandlerFactory, INotificationManager,
};

pub struct NotificationManager {
    already_alerting: HashMap<String, SystemTime>,
    notifiers: Vec<Box<dyn INotificationHandler>>,
    start_msg_required: bool,
    start_msg_sent: bool,
    no_data_required: bool,
    bus_err_required: bool,
    bus_err_interval: Duration,
    no_data_interval: Duration,
}

#[inline]
fn create_notifier(
    factories: &[Box<dyn INotificationHandlerFactory>],
    conf: &NotificationConfig,
) -> Box<dyn INotificationHandler> {
    for factory in factories {
        if let Some(val) = factory.produce(conf) {
            return val;
        }
    }
    unreachable!()
}

impl NotificationManager {
    pub fn new(factories: Vec<Box<dyn INotificationHandlerFactory>>, config: &MainConfig) -> Self {
        // self.already_alerting.clear();
        // let factories = notification_handlers::all();
        let mut start_msg_required = false;
        let mut bus_err_required = false;
        let mut no_data_required = false;
        let mut bus_err_interval = Duration::default();
        let mut no_data_interval = Duration::default();
        let mut notifiers = Vec::new();
        for conf in config.notifications.iter() {
            match conf.kind {
                NotificationKind::Start => start_msg_required = true,
                NotificationKind::BusError => {
                    bus_err_required = true;
                    bus_err_interval = Duration::from_secs_f64(conf.interval);
                }
                NotificationKind::NoData => {
                    no_data_required = true;
                    no_data_interval = Duration::from_secs_f64(conf.interval);
                }
                _ => notifiers.push(create_notifier(&factories, conf)),
            }
        }
        Self {
            already_alerting: Default::default(),
            notifiers,
            start_msg_required,
            start_msg_sent: false,
            no_data_required,
            bus_err_required,
            bus_err_interval,
            no_data_interval,
        }
    }

    fn get_ok_notifications(&self, new_alerts: &HashSet<String>) -> Vec<Notification> {
        let mut result = Vec::new();
        if new_alerts.contains(StdNames::NO_DATA) || new_alerts.contains(StdNames::BUS_ERROR) {
            return result;
        }
        for alert in self.already_alerting.keys() {
            if !new_alerts.contains(alert) {
                result.push(Notification::ok(alert));
            }
        }
        result
    }

    fn update_alerts(&mut self, new_alerts: Vec<(Notification, Duration)>) -> NotificationUpdate {
        let mut as_set = HashSet::with_capacity(new_alerts.len());
        for (alert, _) in new_alerts.iter() {
            as_set.insert(alert.name.clone());
            assert_eq!(alert.state, NotificationState::Alert);
        }
        let mut ok_to_send = self.get_ok_notifications(&as_set);
        if self.start_msg_required && !self.start_msg_sent {
            ok_to_send.push(Notification::ok(StdNames::START));
        }
        let mut alerts_to_send = Vec::new();
        let now = SystemTime::now();
        for (alert, interval) in new_alerts {
            let last_alert = match self.already_alerting.get(&alert.name) {
                Some(val) => *val,
                None => SystemTime::UNIX_EPOCH,
            };
            if now.duration_since(last_alert).unwrap() > interval {
                alerts_to_send.push(alert)
            }
        }
        for ok in ok_to_send.iter() {
            self.already_alerting.remove(&ok.name);
        }
        for alert in alerts_to_send.iter() {
            if !self.already_alerting.contains_key(&alert.name) {
                self.already_alerting
                    .insert(alert.name.clone(), SystemTime::UNIX_EPOCH);
            }
        }
        NotificationUpdate {
            ok: ok_to_send,
            alert: alerts_to_send,
        }
    }

    #[inline]
    fn create_new_alerts(&self, status: &Status) -> Vec<(Notification, Duration)> {
        let mut alerts = Vec::new();
        for notifier in self.notifiers.iter() {
            if let Some(notif) = notifier.process_status(status) {
                alerts.push(notif)
            }
        }
        alerts
    }
}

impl INotificationManager for NotificationManager {
    fn process_status(&mut self, status: &Status) -> NotificationUpdate {
        let alerts = self.create_new_alerts(status);
        self.update_alerts(alerts)
    }

    fn process_no_data(&mut self) -> NotificationUpdate {
        if !self.no_data_required {
            return NotificationUpdate::default();
        }
        let alert = Notification::alert_text(StdNames::NO_DATA, "no data from core");
        self.update_alerts(vec![(alert, self.no_data_interval)])
    }

    fn process_bus_error(&mut self, err: BusError) -> NotificationUpdate {
        if !self.bus_err_required {
            return NotificationUpdate::default();
        }
        let alert = Notification::alert_err(StdNames::BUS_ERROR, err);
        self.update_alerts(vec![(alert, self.bus_err_interval)])
    }

    fn post_send_action(&mut self, notification: &Notification) {
        if notification.name == StdNames::START {
            self.start_msg_sent = true;
        }
        if matches!(notification.state, NotificationState::Ok) {
            return;
        }
        self.already_alerting
            .insert(notification.name.clone(), SystemTime::now());
    }
}
