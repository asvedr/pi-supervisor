use std::fmt::Debug;
use std::io::{Read, Write};
use std::os::unix::net::UnixStream;
use std::time::Duration;

use crate::entities::config::{IOConfig, MainConfig};
use crate::entities::io::{IOCommand, Rights};
use crate::proto::core::IProcess;
use crate::proto::io::{ICommandParser, IIOCommandExecutor};
use crate::utils::system::listen_socket;

const OK_PREFIX: &str = "ok";
const ERR_PREFIX: &str = "er";
const SEPARATOR: &str = "|";

pub struct SocketIOProcess<P: ICommandParser, E: IIOCommandExecutor> {
    path: String,
    parser: P,
    executor: E,
    timeout: Duration,
}

impl<P: ICommandParser, E: IIOCommandExecutor> SocketIOProcess<P, E> {
    pub fn new(parser: P, executor: E, config: &MainConfig) -> Option<Self> {
        let found = config
            .io
            .iter()
            .find(|cnf| matches!(cnf, IOConfig::Socket(_)));
        let cnf = match found {
            None => return None,
            Some(IOConfig::Socket(val)) => val,
            Some(_) => unreachable!(),
        };
        Some(Self {
            path: cnf.path.clone(),
            parser,
            executor,
            timeout: Duration::from_secs_f64(cnf.timeout),
        })
    }

    fn handle_client(&self, mut stream: UnixStream) -> bool {
        let mut buffer = String::new();
        let _ = stream.read_to_string(&mut buffer);
        let (outpath, cmd) = Self::split_buffer(&buffer);
        if outpath.is_empty() {
            eprintln!("outsocket not set");
            return false;
        }
        let out = match UnixStream::connect(outpath) {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Can not open out({}): {:?}", outpath, err);
                return false;
            }
        };
        let _ = out.set_write_timeout(Some(self.timeout));
        let cmd = match self.parser.parse_command(cmd) {
            Ok(IOCommand::Help) => {
                Self::write_ok(out, self.get_help());
                return false;
            }
            Ok(IOCommand::StopSelf) => return true,
            Ok(val) => val,
            Err(err) => {
                Self::write_err(out, err);
                return false;
            }
        };
        match self.executor.execute_command(cmd, Rights::default()) {
            Ok(val) => Self::write_ok(out, val),
            Err(err) => Self::write_err(out, err),
        }
        false
    }

    fn get_help(&self) -> String {
        self.parser.show_help().join("\n")
    }

    fn split_buffer(buffer: &str) -> (&str, &str) {
        if let Some(index) = buffer.find(SEPARATOR) {
            (&buffer[..index], &buffer[index + 1..])
        } else {
            ("", buffer)
        }
    }

    fn write_ok(mut socket: UnixStream, msg: String) {
        let _ = write!(socket, "{}|{}", OK_PREFIX, msg);
    }

    fn write_err<Err: Debug>(mut socket: UnixStream, err: Err) {
        let _ = write!(socket, "{}|{:?}", ERR_PREFIX, err);
    }
}

impl<P: ICommandParser, E: IIOCommandExecutor> IProcess for SocketIOProcess<P, E> {
    fn name(&self) -> &str {
        "socket_io"
    }

    fn run(&mut self) {
        let listener = match listen_socket(&self.path) {
            Ok(val) => val,
            Err(err) => {
                eprintln!("Can't connect socket({}): {:?}", self.path, err);
                return;
            }
        };
        for stream in listener.incoming() {
            let stop = match stream {
                Ok(stream) => self.handle_client(stream),
                Err(err) => {
                    eprintln!("Stream error: {:?}", err);
                    false
                }
            };
            if stop {
                return;
            }
        }
    }
}
