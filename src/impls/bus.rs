use std::collections::VecDeque;
use std::mem;
use std::sync::{Arc, Mutex};

use crate::entities::errors::BusError;
use crate::proto::core::IBus;

#[derive(Clone)]
pub struct Bus<T> {
    max_size: usize,
    inner: Arc<Mutex<PrivBus<T>>>,
}

struct PrivBus<T> {
    max_size: usize,
    data: VecDeque<T>,
}

impl<T: Clone> Bus<T> {
    pub fn new(max_size: usize) -> Self {
        let priv_bus = PrivBus {
            max_size,
            data: VecDeque::new(),
        };
        Self {
            max_size,
            inner: Arc::new(Mutex::new(priv_bus)),
        }
    }
}

impl<T: Clone> IBus<T> for Bus<T> {
    fn max_len(&self) -> usize {
        self.max_size
    }

    fn push(&self, value: T) -> Result<(), BusError> {
        self.inner.lock()?.push(value);
        Ok(())
    }

    fn pop(&self) -> Result<Option<T>, BusError> {
        Ok(self.inner.lock()?.pop())
    }

    fn peek(&self, count: usize) -> Result<Vec<T>, BusError> {
        Ok(self.inner.lock()?.peek(count))
    }

    fn filter(&self, predicate: &dyn Fn(&T) -> bool) -> Result<(), BusError> {
        self.inner.lock()?.filter(predicate);
        Ok(())
    }
}

impl<T: Clone> PrivBus<T> {
    fn push(&mut self, value: T) {
        self.data.push_back(value);
        if self.data.len() > self.max_size {
            self.data.pop_front();
        }
    }

    fn pop(&mut self) -> Option<T> {
        self.data.pop_front()
    }

    fn peek(&mut self, count: usize) -> Vec<T> {
        let mut result = Vec::with_capacity(count);
        for item in self.data.iter() {
            if result.len() >= count {
                break;
            }
            result.push(item.clone());
        }
        result
    }

    fn filter(&mut self, predicate: &dyn Fn(&T) -> bool) {
        let filtered = mem::take(&mut self.data).into_iter().filter(predicate);
        self.data.extend(filtered);
    }
}
