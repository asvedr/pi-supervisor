use std::collections::HashMap;
use std::env::set_var;
use std::path::Path;
use std::process::{Child, Command};
use std::time::SystemTime;

use crate::entities::config::{MainConfig, ServiceConfig};
use crate::entities::constants::SHELL;
use crate::entities::errors::StartStopError;
use crate::entities::info::ProcIntInfo;
use crate::proto::core::{IKiller, IStartStop};

struct Process {
    handler: Child,
    created_at: SystemTime,
}

struct Schema {
    command: String,
    vars: Vec<(String, String)>,
}

impl From<&ServiceConfig> for Schema {
    fn from(value: &ServiceConfig) -> Self {
        Self {
            command: value.command.trim().to_string(),
            vars: value.vars.clone(),
        }
    }
}

pub struct StartStop<K> {
    services: HashMap<String, Process>,
    schema: HashMap<String, Schema>,
    killer: K,
}

impl<K: IKiller> StartStop<K> {
    pub fn new(killer: K, config: &MainConfig) -> Self {
        let schema = config
            .services
            .iter()
            .map(|ser| (ser.name.clone(), Schema::from(ser)))
            .collect();
        Self {
            schema,
            services: Default::default(),
            killer,
        }
    }

    fn make_command(schema: &str) -> Command {
        if Path::new(schema).is_file() {
            return Command::new(schema);
        }
        let mut cmd = Command::new(SHELL);
        cmd.arg("-c").arg(schema);
        cmd
    }

    fn set_vars(vars: &[(String, String)]) {
        for (key, val) in vars {
            set_var(key, val)
        }
    }
}

impl<K: IKiller> IStartStop for StartStop<K> {
    fn start_service(&mut self, key: &str) -> Result<(), StartStopError> {
        if self.services.contains_key(key) {
            return Err(StartStopError::AlreadyRun);
        }
        let schema = match self.schema.get(key) {
            Some(val) => val,
            _ => return Err(StartStopError::NotFound),
        };
        Self::set_vars(&schema.vars);
        let handler = Self::make_command(&schema.command)
            .spawn()
            .map_err(|err| StartStopError::CantSpawn(err.to_string()))?;
        self.services.insert(
            key.to_string(),
            Process {
                handler,
                created_at: SystemTime::now(),
            },
        );
        Ok(())
    }

    fn stop_service(&mut self, key: &str) -> Result<(), StartStopError> {
        if let Some(child) = self.services.get_mut(key) {
            self.killer.kill(&mut child.handler).map_err(|err| {
                let msg = format!("{:?}", err);
                eprintln!("Stop service error: {}", msg);
                StartStopError::CantKill(msg)
            })?
        } else {
            return Err(StartStopError::AlreadyStopped);
        };
        self.services.remove(key);
        Ok(())
    }

    fn restart_service(&mut self, key: &str) -> Result<(), StartStopError> {
        match self.stop_service(key) {
            Ok(_) | Err(StartStopError::AlreadyStopped) => (),
            Err(err) => return Err(err),
        }
        self.start_service(key)
    }

    fn get_pid(&self, key: &str) -> Option<u32> {
        let proc = self.services.get(key)?;
        Some(proc.handler.id())
    }

    fn alive_services(&mut self) -> Vec<ProcIntInfo> {
        let mut result = Vec::with_capacity(self.services.len());
        let now = SystemTime::now();
        for (key, child) in self.services.iter() {
            let info = ProcIntInfo {
                service: key.clone(),
                pid: child.handler.id(),
                lifetime: now.duration_since(child.created_at).unwrap().as_secs(),
            };
            result.push(info);
        }
        result
    }
}
