use std::collections::HashMap;
use std::fs::{read_dir, File};
use std::io::Read;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use crate::entities::errors::TreeBuilderError;
use crate::proto::core::IProcTreeBuilder;

pub struct ProcTreeBuilder {
    pub target_dir: PathBuf,
}

impl Default for ProcTreeBuilder {
    fn default() -> Self {
        Self {
            target_dir: "/proc/".into(),
        }
    }
}

type Matrix = HashMap<u32, Vec<u32>>;

impl ProcTreeBuilder {
    fn make_matrix(&self) -> Result<Matrix, TreeBuilderError> {
        let mut matrix = Matrix::default();
        let mut paths = read_dir(&self.target_dir)
            .map_err(|_| TreeBuilderError::CanNotReadProcDir)?
            .into_iter()
            .map(|entry| Ok(entry?.path()))
            .collect::<Result<Vec<PathBuf>, std::io::Error>>()?;
        paths.sort();
        for path in paths {
            let os_str = match path.file_name() {
                None => return Err(TreeBuilderError::InvalidProcEntry(format!("{:?}", path))),
                Some(val) => val,
            };
            let filename = match os_str.to_str() {
                None => return Err(TreeBuilderError::InvalidProcEntry(format!("{:?}", path))),
                Some(val) => val,
            };
            let pid = match u32::from_str(filename) {
                Ok(val) => val,
                Err(_) => continue,
            };
            let filepath = self.target_dir.join(filename).join("status");
            let parent_pid = Self::get_parent_pid(&filepath)?;
            Self::insert(&mut matrix, parent_pid, pid);
        }
        Ok(matrix)
    }

    fn insert(matrix: &mut Matrix, parent: u32, child: u32) {
        if let Some(vec) = matrix.get_mut(&parent) {
            vec.push(child);
            return;
        }
        matrix.insert(parent, vec![child]);
    }

    fn get_parent_pid(path: &Path) -> Result<u32, TreeBuilderError> {
        let mut file = match File::open(path) {
            Ok(val) => val,
            Err(_) => return Err(TreeBuilderError::CanNotOpenFile(path.to_path_buf())),
        };
        let mut data = String::new();
        file.read_to_string(&mut data)?;
        let key = "PPid";
        for line in data.split('\n') {
            if !line.starts_with(key) {
                continue;
            }
            let pid_str = line[key.len() + 1..].trim();
            let pid = u32::from_str(pid_str)
                .map_err(|_| TreeBuilderError::InvalidPid(pid_str.to_string()))?;
            return Ok(pid);
        }
        Err(TreeBuilderError::ParentPidNotFound)
    }

    fn get_children(matrix: &Matrix, pid: u32) -> &[u32] {
        match matrix.get(&pid) {
            None => &[],
            Some(val) => val,
        }
    }

    fn collect_children(
        storage: &mut Vec<u32>,
        processed: &[u32],
        matrix: &Matrix,
        pid: u32,
    ) -> Result<(), TreeBuilderError> {
        for child in Self::get_children(matrix, pid) {
            // protection from potential cycles
            if processed.contains(child) {
                return Err(TreeBuilderError::LoopDetected);
            }
            storage.push(*child);
        }
        Ok(())
    }
}

impl IProcTreeBuilder for ProcTreeBuilder {
    // result: [process itself .. lowest children]
    fn get_all_children(&self, process: u32) -> Result<Vec<u32>, TreeBuilderError> {
        let mut result = Vec::new();
        let mut current = vec![process];
        let matrix = self.make_matrix()?;
        while !current.is_empty() {
            let mut new_level = Vec::new();
            for pid in current {
                result.push(pid);
                Self::collect_children(&mut new_level, &result, &matrix, pid)?;
            }
            current = new_level;
        }
        Ok(result)
    }
}
