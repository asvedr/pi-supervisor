use futures::StreamExt;
use telegram_bot_ars::{Api, CanReplySendMessage, Error, GetFile, MessageKind, UpdateKind, UserId};

use crate::entities::config::{IOConfig, MainConfig};
use crate::entities::io::{IOCommand, Rights};
use crate::proto::core::IProcess;
use crate::proto::io::{ICommandParser, IIOCommandExecutor};
use crate::utils::system::download_file;
use crate::utils::telegram::make_download_link;

pub struct TgIOProcess<P: ICommandParser, E: IIOCommandExecutor> {
    parser: P,
    executor: E,
    token: String,
    admin_id: i64,
    rights: Rights,
}

impl<P: ICommandParser, E: IIOCommandExecutor> TgIOProcess<P, E> {
    pub fn new(parser: P, executor: E, config: &MainConfig) -> Option<Self> {
        let found = config.io.iter().find(|cnf| matches!(cnf, IOConfig::Tg(_)));
        let cnf = match found {
            None => return None,
            Some(IOConfig::Tg(val)) => val,
            _ => unreachable!(),
        };
        Some(Self {
            parser,
            executor,
            token: cnf.token.clone(),
            admin_id: cnf.admin_id,
            rights: Rights {
                shell: cnf.allow_shell,
                loading: cnf.allow_loading,
            },
        })
    }

    async fn arun(&mut self) -> Result<(), Error> {
        let api = Api::new(&self.token);
        let mut stream = api.stream();
        while let Some(update) = stream.next().await {
            // If the received update contains a new message...
            let update = update?;
            let message = match update.kind {
                Some(UpdateKind::Message(val)) => val,
                _ => continue,
            };
            if message.from.id != UserId::new(self.admin_id) {
                continue;
            }
            let resp = match message.kind {
                MessageKind::Text { ref data, .. } => self.execute_text(data),
                MessageKind::Document {
                    ref data,
                    ref caption,
                } => {
                    let file = api.send(GetFile::new(data)).await?;
                    let file_path = match file.file_path {
                        Some(ref val) => val,
                        _ => continue,
                    };
                    let link = make_download_link(&self.token, file_path);
                    self.execute_file(caption.clone(), link)
                }
                _ => continue,
            };
            if !resp.is_empty() {
                api.send(message.text_reply(resp)).await?;
            }
        }
        Ok(())
    }

    fn execute_text(&self, text: &str) -> String {
        if !text.starts_with('/') {
            return "".to_string();
        }
        let cmd = match self.parser.parse_command(&text[1..]) {
            Ok(IOCommand::Help) => return self.get_help(),
            Ok(val) => val,
            Err(err) => return format!("ERR: {:?}", err),
        };
        match self.executor.execute_command(cmd, self.rights) {
            Ok(val) => val,
            Err(err) => format!("ERR: {:?}", err),
        }
    }

    fn execute_file(&self, opt_name: Option<String>, link: String) -> String {
        if !self.rights.loading {
            return "Not allowed".to_string();
        }
        let name = match opt_name {
            Some(val) => val,
            _ => return "Path not set".to_string(),
        };
        match download_file(&name, &link) {
            Ok(_) => format!("saved to: {:?}", name),
            Err(err) => format!("Error: {:?}", err),
        }
    }

    fn get_help(&self) -> String {
        self.parser
            .show_help()
            .iter()
            .map(|s| format!("/{}", s))
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl<P: ICommandParser, E: IIOCommandExecutor> IProcess for TgIOProcess<P, E> {
    fn name(&self) -> &str {
        "tg_io"
    }

    fn run(&mut self) {
        loop {
            let status = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(self.arun());
            if let Err(err) = status {
                eprintln!("tg io error: {:?}", err)
            }
        }
    }
}
