use std::collections::HashSet;
use std::process;
use std::time::{Duration, SystemTime};

use crate::entities::command::CoreCommand;
use crate::entities::config::MainConfig;
use crate::entities::errors::CoreProcessErrorKind as CPEK;
use crate::entities::errors::{CoreProcessError, GetInfoError};
use crate::entities::info::{ProcExtInfo, ProcIntInfo, ProcTotalInfo, Status};
use crate::proto::core::{ICoreBuses, IInfoCollector, IProcess, IStartStop, IThresholdKiller};
use crate::utils::system::reboot;

pub struct CoreProcess<SI: IInfoCollector, SS: IStartStop, B: ICoreBuses, Tk: IThresholdKiller> {
    pub sys_info: SI,
    pub start_stop: SS,
    pub buses: B,
    pub killer: Tk,
    all_service_keys: Vec<String>,
    // list of inner id
    must_be_alive: HashSet<usize>,
    sleep: Duration,
    error_lifetime: Duration,
    created_at: SystemTime,
}

impl<SI: IInfoCollector, SS: IStartStop, B: ICoreBuses, Tk: IThresholdKiller>
    CoreProcess<SI, SS, B, Tk>
{
    pub fn new(sys_info: SI, start_stop: SS, buses: B, killer: Tk, config: &MainConfig) -> Self {
        let mut all_service_keys: Vec<String> = Vec::new();
        let mut must_be_alive = HashSet::new();
        let mut start_stop_params = Vec::new();
        for service in config.services.iter() {
            let local_id = all_service_keys.len();
            all_service_keys.push(service.name.clone());
            start_stop_params.push((service.name.clone(), service.command.clone()));
            if service.start {
                must_be_alive.insert(local_id);
            }
        }
        Self {
            sys_info,
            start_stop,
            buses,
            killer,
            all_service_keys,
            must_be_alive,
            sleep: Duration::from_secs_f64(config.core_sleep),
            error_lifetime: Duration::from_secs_f64(config.error_lifetime),
            created_at: SystemTime::now(),
        }
    }

    fn log_err(&mut self, event: Result<(), CoreProcessError>) {
        let err = match event {
            Ok(_) => return,
            Err(err) => err,
        };
        if let Err(err) = self.buses.error().push(err) {
            panic!("core: err bus error: {:?}", err)
        }
    }

    fn process_command(&mut self, command: CoreCommand) {
        let res = match command {
            CoreCommand::Start(key) => self.start_service_cmd(key),
            CoreCommand::Stop(key) => self.stop_service_cmd(&key),
            CoreCommand::Reboot => self.reboot(),
            CoreCommand::StopSelf => unreachable!(),
        };
        self.log_err(res);
    }

    fn get_service_local_id(&self, name: &str) -> Result<usize, CoreProcessError> {
        for (i, service) in self.all_service_keys.iter().enumerate() {
            if service == name {
                return Ok(i);
            }
        }
        Err(CPEK::InvalidService(name.to_string()).into())
    }

    fn start_service_cmd(&mut self, name: String) -> Result<(), CoreProcessError> {
        let local_id = self.get_service_local_id(&name)?;
        self.must_be_alive.insert(local_id);
        Ok(())
    }

    fn stop_service_cmd(&mut self, name: &str) -> Result<(), CoreProcessError> {
        let local_id = self.get_service_local_id(name)?;
        self.must_be_alive.remove(&local_id);
        Ok(())
    }

    fn stop_service_by_id(&mut self, local_id: usize) {
        let name = &self.all_service_keys[local_id];
        let evt = self
            .start_stop
            .stop_service(name)
            .map_err(|err| CPEK::StartStopError(name.clone(), err).into());
        self.log_err(evt)
    }

    fn start_service_by_id(&mut self, key: usize) {
        let name = &self.all_service_keys[key];
        let err = match self.start_stop.restart_service(name) {
            Ok(_) => return,
            Err(val) => val,
        };
        let evt = Err(CoreProcessError::new(CPEK::StartStopError(
            name.clone(),
            err,
        )));
        self.log_err(evt);
    }

    fn reboot(&mut self) -> Result<(), CoreProcessError> {
        Ok(reboot().map_err(CPEK::CanNotReboot)?)
    }

    fn self_info(&self) -> ProcTotalInfo {
        let self_pid = process::id();
        let mem_used = match self.sys_info.get_proc_info(self_pid) {
            None => Err(GetInfoError::CantGet),
            Some(val) => val.mem_used,
        };
        ProcTotalInfo {
            mem_used,
            alive: Ok(true),
            service: "<self>".to_string(),
            pid: self_pid,
            lifetime: SystemTime::now()
                .duration_since(self.created_at)
                .unwrap()
                .as_secs(),
            ..Default::default()
        }
    }

    fn collect_status(&mut self) -> Status {
        let sys_info = self.sys_info.get_sys_info();
        let mut alive = self.start_stop.alive_services();
        alive.sort_by(|a, b| a.service.cmp(&b.service));
        let mut service_statuses = vec![self.self_info()];
        for (index, service) in self.all_service_keys.iter().enumerate() {
            let found = alive.binary_search_by(|ser| ser.service.cmp(service));
            let service_status = if let Ok(i) = found {
                self.make_status_for_alive(index, &alive[i])
            } else {
                self.make_status_for_dead(index, service)
            };
            service_statuses.push(service_status);
        }
        Status {
            sys_info,
            services: service_statuses,
            terminate_all: false,
            ..Default::default()
        }
    }

    fn make_status_for_alive(&self, local_id: usize, info: &ProcIntInfo) -> ProcTotalInfo {
        let ext_info = match self.sys_info.get_proc_info(info.pid) {
            Some(val) => val,
            None => ProcExtInfo {
                mem_used: Err(GetInfoError::NotAvailable),
                alive: Err(GetInfoError::NotAvailable),
            },
        };
        let must_be_alive = self.must_be_alive.contains(&local_id);
        ProcTotalInfo {
            mem_used: ext_info.mem_used,
            alive: Ok(true),
            service: info.service.clone(),
            pid: info.pid,
            lifetime: info.lifetime,
            start_required: false,
            stop_required: !must_be_alive,
            local_id,
        }
    }

    fn make_status_for_dead(&self, local_id: usize, name: &str) -> ProcTotalInfo {
        let must_be_alive = self.must_be_alive.contains(&local_id);
        ProcTotalInfo {
            mem_used: Ok(0),
            alive: Ok(false),
            service: name.to_string(),
            pid: 0,
            lifetime: 0,
            start_required: must_be_alive,
            stop_required: false,
            local_id,
        }
    }

    fn send_stop_status(&self) {
        let _ = self.buses.status().push(Status {
            terminate_all: true,
            ..Default::default()
        });
    }

    fn clear_old_errors(&self) {
        let bus = self.buses.error();
        let now = SystemTime::now();
        let res = bus.filter(&|err: &CoreProcessError| {
            now.duration_since(err.when).unwrap() < self.error_lifetime
        });
        if let Err(err) = res {
            eprintln!("core: err bus error: {:?}", err);
        }
    }

    #[inline]
    fn extract_services_to_restart(status: &Status) -> Vec<usize> {
        let mut result = Vec::new();
        for service in status.services.iter() {
            if service.start_required {
                result.push(service.local_id)
            }
        }
        result
    }

    #[inline]
    fn extract_services_to_stop(status: &Status) -> Vec<usize> {
        let mut result = Vec::new();
        for service in status.services.iter() {
            if service.stop_required {
                result.push(service.local_id)
            }
        }
        result
    }
}

impl<SI: IInfoCollector, SS: IStartStop, B: ICoreBuses, Tk: IThresholdKiller> IProcess
    for CoreProcess<SI, SS, B, Tk>
{
    fn name(&self) -> &str {
        "core"
    }

    fn iteration(&mut self) -> bool {
        self.clear_old_errors();
        match self.buses.command().pop() {
            Ok(Some(CoreCommand::StopSelf)) => {
                self.send_stop_status();
                return false;
            }
            Ok(Some(cmd)) => self.process_command(cmd),
            Ok(None) => (),
            Err(err) => panic!("core: cmd bus error: {:?}", err),
        }
        let mut status = self.collect_status();
        let service_names_to_kill = self.killer.find_services_to_kill(&status);
        let service_ids_to_kill = service_names_to_kill
            .iter()
            .map(|s| match self.get_service_local_id(s) {
                Ok(val) => val,
                _ => panic!("killer returns invalid name: {:?}", s),
            })
            .collect::<Vec<_>>();
        let services_to_restart = Self::extract_services_to_restart(&status);
        let services_to_stop = Self::extract_services_to_stop(&status);

        status.killed = service_names_to_kill;
        if let Err(err) = self.buses.status().push(status) {
            panic!("core: status bus error: {:?}", err)
        };
        for service in services_to_stop {
            self.stop_service_by_id(service)
        }
        for service in services_to_restart {
            if !service_ids_to_kill.contains(&service) {
                self.start_service_by_id(service);
            }
        }
        for service in service_ids_to_kill {
            self.stop_service_by_id(service);
        }
        std::thread::sleep(self.sleep);
        true
    }
}
