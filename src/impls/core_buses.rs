use crate::entities::command::CoreCommand;
use crate::entities::errors::CoreProcessError;
use crate::entities::info::Status;
use crate::proto::core::{IBus, ICoreBuses};

#[derive(Clone)]
pub struct CoreBuses<C, S, E> {
    pub command: C,
    pub status: S,
    pub error: E,
}

impl<C: IBus<CoreCommand> + Clone, S: IBus<Status> + Clone, E: IBus<CoreProcessError> + Clone>
    ICoreBuses for CoreBuses<C, S, E>
{
    fn command(&self) -> &dyn IBus<CoreCommand> {
        &self.command
    }

    fn status(&self) -> &dyn IBus<Status> {
        &self.status
    }

    fn error(&self) -> &dyn IBus<CoreProcessError> {
        &self.error
    }
}
