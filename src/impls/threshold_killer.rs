use crate::entities::config::{KillReason, MainConfig};
use crate::entities::info::{ProcTotalInfo, Status};
use crate::proto::core::IThresholdKiller;

pub struct ThresholdKiller {
    name_reasons: Vec<(String, Vec<KillReason>)>,
}

impl ThresholdKiller {
    pub fn new(config: &MainConfig) -> Self {
        let name_reasons = config
            .services
            .iter()
            .filter(|s| !s.kill_on.is_empty())
            .map(|s| (s.name.clone(), s.kill_on.clone()))
            .collect::<Vec<_>>();
        Self { name_reasons }
    }

    fn get_service_params(&self, name: &str) -> Option<&[KillReason]> {
        for (k, v) in self.name_reasons.iter() {
            if k == name {
                return Some(v);
            }
        }
        None
    }

    fn should_kill(&self, temp: f64, free_mem: Option<u64>, service: &ProcTotalInfo) -> bool {
        let params = match self.get_service_params(&service.service) {
            Some(val) => val,
            _ => return false,
        };
        for param in params {
            if Self::check_param(param, service, temp, free_mem) {
                return true;
            }
        }
        false
    }

    #[inline]
    fn check_param(
        param: &KillReason,
        service: &ProcTotalInfo,
        temp: f64,
        free_mem: Option<u64>,
    ) -> bool {
        match param {
            KillReason::TempGt(threshold) => temp > *threshold,
            KillReason::ProcMemGt(threshold) => match service.mem_used {
                Ok(val) => val > *threshold,
                _ => false,
            },
            KillReason::FreeMemLt(threshold) => match free_mem {
                Some(val) => val < *threshold,
                _ => false,
            },
        }
    }
}

impl IThresholdKiller for ThresholdKiller {
    fn find_services_to_kill(&self, status: &Status) -> Vec<String> {
        #[allow(clippy::manual_unwrap_or)]
        let temp = match status.sys_info.temp {
            Ok(val) => val,
            Err(_) => 0.0,
        };
        let free_mem = match status.sys_info.mem_free {
            Ok(val) => Some(val),
            Err(_) => None,
        };
        let mut result = Vec::new();
        for service in status.services.iter() {
            if self.should_kill(temp, free_mem, service) {
                result.push(service.service.clone())
            }
        }
        result
    }
}
