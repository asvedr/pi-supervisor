use std::fs::File;
use std::io::Read;
use std::str::FromStr;

use yaml_rust::yaml::Hash;
use yaml_rust::{Yaml, YamlLoader};

use crate::entities::config::{
    IOConfig, KillReason, MainConfig, NotificationConfig, NotificationKind, NotificatorConfig,
    ServiceConfig, SocketConfig, TelegramConfig,
};
use crate::entities::constants::{
    DEFAULT_ERROR_LIFETIME, DEFAULT_NOTIFICATION_INTERVAL, DEFAULT_SLEEP, DEFAULT_SYS_TEMP_SOURCE,
};
use crate::entities::errors::ConfigParserError;
use crate::proto::config::IConfigParser;

pub struct ConfigParser;

type SStr = &'static str;
type PRes<T> = Result<T, ConfigParserError>;

fn unwrap_or_raise<T, E>(val: Option<T>, err: E) -> Result<T, E> {
    match val {
        Some(val) => Ok(val),
        _ => Err(err),
    }
}

fn get_field(dict: &Hash, field: SStr, msg: Option<SStr>) -> PRes<&Yaml> {
    match dict.get(&Yaml::String(field.to_string())) {
        None => Err(ConfigParserError::FieldNotFound(msg.unwrap_or(field))),
        Some(val) => Ok(val),
    }
}

fn or_default<T>(result: PRes<T>, default: T) -> PRes<T> {
    match result {
        Ok(val) => Ok(val),
        Err(ConfigParserError::FieldNotFound(_)) => Ok(default),
        Err(err) => Err(err),
    }
}

fn get_f64(dict: &Hash, field: SStr, msg: Option<SStr>) -> PRes<f64> {
    let val = get_field(dict, field, msg)?;
    match (val.as_f64(), val.as_i64()) {
        (Some(x), _) => Ok(x),
        (_, Some(x)) => Ok(x as f64),
        _ => Err(ConfigParserError::InvalidField(msg.unwrap_or(field))),
    }
}

fn get_int(dict: &Hash, field: SStr, msg: Option<SStr>) -> PRes<i64> {
    unwrap_or_raise(
        get_field(dict, field, msg)?.as_i64(),
        ConfigParserError::InvalidField(msg.unwrap_or(field)),
    )
}

fn get_str(dict: &Hash, field: SStr, msg: Option<SStr>) -> PRes<String> {
    let lnk = unwrap_or_raise(
        get_field(dict, field, msg)?.as_str(),
        ConfigParserError::ObjectNotStr(msg.unwrap_or(field)),
    )?;
    Ok(lnk.to_string())
}

fn get_bool(dict: &Hash, field: SStr, msg: Option<SStr>) -> PRes<bool> {
    unwrap_or_raise(
        get_field(dict, field, msg)?.as_bool(),
        ConfigParserError::ObjectNotBool(msg.unwrap_or(field)),
    )
}

fn parse_list<T>(yml: &Yaml, func: impl Fn(&Yaml) -> PRes<T>, msg: &'static str) -> PRes<Vec<T>> {
    let vec = match yml.as_vec() {
        Some(val) => val,
        None => return Err(ConfigParserError::InvalidField(msg)),
    };
    vec.iter().map(func).collect::<Result<Vec<_>, _>>()
}

impl ConfigParser {
    fn parse_kill_reason(yml: &Yaml) -> PRes<KillReason> {
        let err = ConfigParserError::InvalidField("service.kill_on[]");
        let text = unwrap_or_raise(yml.as_str(), err.clone())?;
        KillReason::from_str(text).map_err(|_| err)
    }

    fn parse_kill_on(service: &Hash) -> PRes<Vec<KillReason>> {
        let field = match get_field(service, "kill_on", None) {
            Ok(val) => val,
            Err(ConfigParserError::FieldNotFound(_)) => return Ok(Vec::new()),
            Err(err) => return Err(err),
        };
        parse_list(field, Self::parse_kill_reason, "service.kill_on")
    }

    fn parse_vars(env: &Hash) -> PRes<Vec<(String, String)>> {
        let mut result = Vec::new();
        for (key, val) in env {
            let key = match key {
                Yaml::String(val) => val.clone(),
                Yaml::Real(val) => val.to_string(),
                Yaml::Integer(val) => val.to_string(),
                _ => return Err(ConfigParserError::InvalidField("service.env.key")),
            };
            let val = match val {
                Yaml::String(val) => val.clone(),
                Yaml::Real(val) => val.to_string(),
                Yaml::Integer(val) => val.to_string(),
                _ => return Err(ConfigParserError::InvalidField("service.env.value")),
            };
            result.push((key, val))
        }
        Ok(result)
    }

    fn parse_service(yml: &Yaml) -> PRes<ServiceConfig> {
        let dict = unwrap_or_raise(yml.as_hash(), ConfigParserError::ObjectNotDict("service[]"))?;
        let vars = match get_field(dict, "env", None) {
            Ok(Yaml::Hash(env_dict)) => Self::parse_vars(env_dict)?,
            Ok(_) => return Err(ConfigParserError::InvalidField("service.env")),
            Err(ConfigParserError::FieldNotFound(_)) => Vec::new(),
            Err(err) => return Err(err),
        };
        Ok(ServiceConfig {
            name: get_str(dict, "name", Some("service.name"))?,
            command: get_str(dict, "command", Some("service.command"))?,
            start: or_default(get_bool(dict, "start", Some("service.start")), true)?,
            kill_on: Self::parse_kill_on(dict)?,
            vars,
        })
    }

    fn parse_socket(dict: &Hash) -> PRes<SocketConfig> {
        Ok(SocketConfig {
            path: get_str(dict, "path", Some("io.(type=socket).path"))?,
            timeout: or_default(
                get_f64(dict, "timeout", Some("io.(type=socket).timeout")),
                1.0,
            )?,
        })
    }

    fn parse_tg_io(dict: &Hash) -> PRes<TelegramConfig> {
        Ok(TelegramConfig {
            admin_id: get_int(dict, "admin_id", Some("io.(type=tg).admin_id"))?,
            token: get_str(dict, "token", Some("io.(type=tg).token"))?,
            notification_interval: 0.0,
            allow_shell: or_default(
                get_bool(dict, "allow_shell", Some("io.(type=tg).allow_shell")),
                false,
            )?,
            allow_loading: or_default(
                get_bool(dict, "allow_loading", Some("io.(type=tg).allow_loading")),
                false,
            )?,
        })
    }

    fn parse_io(yml: &Yaml) -> PRes<IOConfig> {
        let dict = unwrap_or_raise(yml.as_hash(), ConfigParserError::ObjectNotDict("io[]"))?;
        match &get_str(dict, "type", Some("io.type"))?[..] {
            "socket" => Ok(IOConfig::Socket(Self::parse_socket(dict)?)),
            "tg" => Ok(IOConfig::Tg(Self::parse_tg_io(dict)?)),
            _ => Err(ConfigParserError::InvalidField("io.type")),
        }
    }

    fn parse_tg_notificator(dict: &Hash) -> PRes<TelegramConfig> {
        let notification_interval = get_f64(
            dict,
            "notification_interval",
            Some("notificators.(type=tg).notification_interval"),
        );
        Ok(TelegramConfig {
            admin_id: get_int(dict, "admin_id", Some("notificators.(type=tg).admin_id"))?,
            token: get_str(dict, "token", Some("notificators.(type=tg).token"))?,
            notification_interval: or_default(
                notification_interval,
                DEFAULT_NOTIFICATION_INTERVAL,
            )?,
            allow_shell: false,
            allow_loading: false,
        })
    }

    fn parse_notificator(yml: &Yaml) -> PRes<NotificatorConfig> {
        let dict = unwrap_or_raise(
            yml.as_hash(),
            ConfigParserError::ObjectNotDict("notificators[]"),
        )?;
        match &get_str(dict, "type", Some("notificators.type"))?[..] {
            "tg" => Ok(NotificatorConfig::Tg(Self::parse_tg_notificator(dict)?)),
            _ => Err(ConfigParserError::InvalidField("notificators.type")),
        }
    }

    fn parse_notificators(dict: &Hash) -> PRes<Vec<NotificatorConfig>> {
        let list = match get_field(dict, "notificators", None) {
            Ok(val) => val,
            Err(ConfigParserError::FieldNotFound(_)) => return Ok(Vec::new()),
            Err(err) => return Err(err),
        };
        parse_list(list, Self::parse_notificator, "notificators")
    }

    fn parse_notification(yml: &Yaml) -> PRes<NotificationConfig> {
        let dict = unwrap_or_raise(
            yml.as_hash(),
            ConfigParserError::ObjectNotDict("notifications[]"),
        )?;
        let kind = get_str(dict, "type", Some("notifications.type"))?;
        let kind = NotificationKind::from_str(&kind)
            .map_err(|_| ConfigParserError::InvalidField("notifications.type"))?;
        Ok(NotificationConfig {
            kind,
            interval: or_default(
                get_f64(dict, "interval", Some("notifications.interval")),
                0.0,
            )?,
        })
    }

    fn parse_notifications(dict: &Hash) -> PRes<Vec<NotificationConfig>> {
        let list = match get_field(dict, "notifications", None) {
            Ok(val) => val,
            Err(ConfigParserError::FieldNotFound(_)) => return Ok(Vec::new()),
            Err(err) => return Err(err),
        };
        parse_list(list, Self::parse_notification, "notifications")
    }

    fn parse_top_level(dict: &Hash) -> PRes<MainConfig> {
        Ok(MainConfig {
            services: parse_list(
                get_field(dict, "services", None)?,
                Self::parse_service,
                "services",
            )?,
            io: parse_list(get_field(dict, "io", None)?, Self::parse_io, "io")?,
            notificators: Self::parse_notificators(dict)?,
            notifications: Self::parse_notifications(dict)?,
            core_sleep: or_default(get_f64(dict, "core_sleep", None), DEFAULT_SLEEP)?,
            notificator_sleep: or_default(get_f64(dict, "notificator_sleep", None), DEFAULT_SLEEP)?,
            io_sleep: or_default(get_f64(dict, "io_sleep", None), DEFAULT_SLEEP)?,
            error_lifetime: or_default(
                get_f64(dict, "error_lifetime", None),
                DEFAULT_ERROR_LIFETIME,
            )?,
            sys_temp_source: or_default(
                get_str(dict, "sys_temp_source", None),
                DEFAULT_SYS_TEMP_SOURCE.to_string(),
            )?,
        })
    }
}

impl IConfigParser for ConfigParser {
    fn parse_file(&self, path: &str) -> PRes<MainConfig> {
        let mut file = File::open(path).map_err(|_| ConfigParserError::CanNotReadFile)?;
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)
            .map_err(|_| ConfigParserError::CanNotReadFile)?;
        self.parse_str(&buffer)
    }

    fn parse_str(&self, data: &str) -> PRes<MainConfig> {
        let docs = YamlLoader::load_from_str(data).map_err(|err| {
            eprintln!("yaml error: {:?}", err);
            ConfigParserError::InvalidYaml
        })?;
        let doc = unwrap_or_raise(docs[0].as_hash(), ConfigParserError::TopLevelNotDict)?;
        Self::parse_top_level(doc)
    }
}
