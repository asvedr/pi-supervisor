use crate::entities::errors::ParserError;
use crate::entities::io::IOCommand;
use crate::proto::io::ICommandParser;

pub struct CommandParser {
    stop_self_available: bool,
}

#[cfg(test)]
impl Default for CommandParser {
    fn default() -> Self {
        Self {
            stop_self_available: true,
        }
    }
}

#[allow(clippy::derivable_impls)]
#[cfg(not(test))]
impl Default for CommandParser {
    fn default() -> Self {
        Self {
            stop_self_available: false,
        }
    }
}

impl ICommandParser for CommandParser {
    fn parse_command(&self, src: &str) -> Result<IOCommand, ParserError> {
        let split = src.split(' ').collect::<Vec<_>>();
        match split[..] {
            ["sh", ..] => Ok(IOCommand::Shell(split[1..].join(" "))),
            ["start", ser] => Ok(IOCommand::StartService(ser.to_string())),
            ["start"] => Err(ParserError::ParamRequired),
            ["stop", "self"] if self.stop_self_available => Ok(IOCommand::StopSelf),
            ["stop", ser] => Ok(IOCommand::StopService(ser.to_string())),
            ["stop"] => Err(ParserError::ParamRequired),
            ["reboot"] => Ok(IOCommand::Reboot),
            ["show", "sys"] => Ok(IOCommand::ShowSysInfo),
            ["show", "all", "services"] => Ok(IOCommand::ShowAllServices),
            ["show", "alive"] => Ok(IOCommand::ShowAliveServices),
            ["show", "errors"] => Ok(IOCommand::ShowErrors),
            ["show"] => Err(ParserError::ParamRequired),
            ["help"] => Ok(IOCommand::Help),
            _ => Err(ParserError::InvalidCommand),
        }
    }

    fn show_help(&self) -> &[&str] {
        &[
            "start <service> - start service",
            "stop <service> - stop service",
            "reboot - reboot machine",
            "show sys - show system information",
            "show all services - show all available services",
            "show alive - show all started services",
            "show errors - show last core errors",
            "help - show this message",
        ]
    }
}
