use std::thread;
use std::time::Duration;

use telegram_bot_ars::{Api, SendMessage, UserId};

use crate::entities::config::{MainConfig, NotificatorConfig, TelegramConfig};
use crate::entities::notifications::{Notification, NotificationState, NotificationUpdate};
use crate::proto::core::{ICoreBuses, IProcess};
use crate::proto::notifications::INotificationManager;

pub struct TgNotificationProcess<NM: INotificationManager, B: ICoreBuses> {
    notif_manager: NM,
    sleep: Duration,
    buses: B,

    admin_id: i64,
    token: String,
}

impl<NM: INotificationManager, B: ICoreBuses> TgNotificationProcess<NM, B> {
    pub fn new(notif_manager: NM, buses: B, config: &MainConfig) -> Option<Self> {
        let self_config = Self::find_self_config(config)?;
        let admin_id = self_config.admin_id;
        assert!(admin_id > 1);
        Some(Self {
            notif_manager,
            buses,
            sleep: Duration::from_secs_f64(config.notificator_sleep),
            admin_id,
            token: self_config.token.clone(),
        })
    }

    fn fetch_notifications(&mut self) -> (NotificationUpdate, bool) {
        match self.buses.status().peek(1) {
            Ok(val) if val.len() == 1 => {
                let status = &val[0];
                let notifs = self.notif_manager.process_status(status);
                (notifs, status.terminate_all)
            }
            Ok(_) => {
                let notifs = self.notif_manager.process_no_data();
                (notifs, false)
            }
            Err(err) => {
                let notifs = self.notif_manager.process_bus_error(err);
                (notifs, false)
            }
        }
    }

    async fn send_notification(&mut self, api: &mut Api, notification: Notification) {
        let state = match notification.state {
            NotificationState::Ok => "✅[ok]",
            NotificationState::Alert => "🔴[alert]",
        };
        let mut msg = format!("{} {}", state, notification.name);
        if !notification.text.is_empty() {
            msg.push_str(":\n");
            msg.push_str(&notification.text);
        }
        let admin = UserId::new(self.admin_id);
        if let Err(err) = api.send(SendMessage::new(admin, msg)).await {
            eprintln!("can not send: {:?}", err);
        } else {
            self.notif_manager.post_send_action(&notification);
        }
    }

    async fn aiteration(&mut self, api: &mut Api) -> bool {
        let (notifications, stop) = self.fetch_notifications();
        for notification in notifications.ok {
            self.send_notification(api, notification).await;
        }
        for notification in notifications.alert {
            self.send_notification(api, notification).await;
        }
        // Async sleep not requred 'cause there is only one coroutine in the thread
        thread::sleep(self.sleep);
        !stop
    }

    #[inline]
    fn find_self_config(config: &MainConfig) -> Option<&TelegramConfig> {
        if let Some(NotificatorConfig::Tg(val)) = config.notificators.first() {
            Some(val)
        } else {
            None
        }
        // for notificator in config.notificators.iter() {
        //     return match notificator {
        //         NotificatorConfig::Tg(val) => Some(val),
        //     };
        // }
        // None
    }
}

async fn main_loop<P: INotificationManager, B: ICoreBuses>(
    notificator: &mut TgNotificationProcess<P, B>,
) {
    let mut api = Api::new(&notificator.token);
    while notificator.aiteration(&mut api).await {}
}

impl<P: INotificationManager, B: ICoreBuses> IProcess for TgNotificationProcess<P, B> {
    fn name(&self) -> &str {
        "tg_notificator"
    }

    fn run(&mut self) {
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(main_loop(self))
    }
}
