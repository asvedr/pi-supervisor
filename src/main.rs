use std::env;
use std::process;

mod app;
mod entities;
mod impls;
mod proto;
mod utils;

use crate::app::config::set_path;
use crate::entities::constants::CONFIG_PATH;

const HELP_TEXT: &str = concat!(
    "raspberry pi supervisor\n",
    "  pi-supervisor -h: show this message\n",
    "  pi-supervisor -v: show version\n",
    "  pi-supervisor config-path: run app with custom config\n",
    "  pi-supervisor: run app with default config"
);

fn get_conf_path() -> String {
    let args = env::args().collect::<Vec<_>>();
    if args.len() == 1 {
        return CONFIG_PATH.to_string();
    }
    if args[1] == "-h" || args[1] == "--help" {
        println!("{}", HELP_TEXT);
        process::exit(0)
    }
    if args[1] == "-v" || args[1] == "--version" {
        let ver = env!("CARGO_PKG_VERSION");
        println!("version: {}", ver);
        process::exit(0)
    }
    args[1].clone()
}

fn main() {
    let path = get_conf_path();
    set_path(path);
    app::application::Application::new().run();
}
