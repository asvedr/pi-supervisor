use std::process::Child;

use crate::entities::command::CoreCommand;
use crate::entities::errors::{
    BusError, CoreProcessError, KillerError, StartStopError, TreeBuilderError,
};
use crate::entities::info::{ProcExtInfo, ProcIntInfo, Status, SysInfo};

pub trait IInfoCollector {
    // fn init(&mut self, sys_temp_source: String);
    fn get_sys_info(&self) -> SysInfo;
    fn get_proc_info(&self, pid: u32) -> Option<ProcExtInfo>;
}

pub trait IStartStop {
    // fn init(&mut self, map: Vec<(String, String)>);
    fn start_service(&mut self, key: &str) -> Result<(), StartStopError>;
    fn stop_service(&mut self, key: &str) -> Result<(), StartStopError>;
    fn restart_service(&mut self, key: &str) -> Result<(), StartStopError>;
    fn get_pid(&self, key: &str) -> Option<u32>;
    fn alive_services(&mut self) -> Vec<ProcIntInfo>;
}

pub trait IKiller {
    fn name(&self) -> &str;
    fn kill(&self, process: &mut Child) -> Result<(), KillerError>;
}

pub trait IProcTreeBuilder {
    // result: [process itself .. lowest children]
    fn get_all_children(&self, process: u32) -> Result<Vec<u32>, TreeBuilderError>;
}

pub trait IProcess {
    // fn init(&mut self, config: &MainConfig);
    fn name(&self) -> &str;
    fn iteration(&mut self) -> bool {
        unimplemented!()
    }
    fn run(&mut self) {
        while self.iteration() {}
    }
}

pub trait IBus<T> {
    fn max_len(&self) -> usize;
    fn push(&self, value: T) -> Result<(), BusError>;
    fn pop(&self) -> Result<Option<T>, BusError>;
    fn peek(&self, count: usize) -> Result<Vec<T>, BusError>;
    fn filter(&self, predicate: &dyn Fn(&T) -> bool) -> Result<(), BusError>;
}

pub trait ICoreBuses: Clone {
    fn command(&self) -> &dyn IBus<CoreCommand>;
    fn status(&self) -> &dyn IBus<Status>;
    fn error(&self) -> &dyn IBus<CoreProcessError>;
}

pub trait IThresholdKiller {
    // fn init(&mut self, config: &MainConfig);
    fn find_services_to_kill(&self, status: &Status) -> Vec<String>;
}
