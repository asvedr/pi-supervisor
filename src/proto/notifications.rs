use crate::entities::config::NotificationConfig;
use crate::entities::errors::BusError;
use crate::entities::info::Status;
use crate::entities::notifications::{Notification, NotificationUpdate};
use std::time::Duration;

pub trait INotificationHandler {
    fn process_status(&self, status: &Status) -> Option<(Notification, Duration)>;
}

pub trait INotificationHandlerFactory {
    fn produce(&self, conf: &NotificationConfig) -> Option<Box<dyn INotificationHandler>>;
}

pub trait INotificationManager {
    fn process_status(&mut self, status: &Status) -> NotificationUpdate;
    fn process_no_data(&mut self) -> NotificationUpdate;
    fn process_bus_error(&mut self, err: BusError) -> NotificationUpdate;
    fn post_send_action(&mut self, notification: &Notification);
}
