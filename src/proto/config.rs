use crate::entities::config::MainConfig;
use crate::entities::errors::ConfigParserError;

pub trait IConfigParser {
    fn parse_file(&self, path: &str) -> Result<MainConfig, ConfigParserError>;
    fn parse_str(&self, data: &str) -> Result<MainConfig, ConfigParserError>;
}
