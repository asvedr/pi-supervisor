use crate::entities::errors::{CommandExecutorError, ParserError};
use crate::entities::io::{IOCommand, Rights};

pub trait ICommandParser {
    fn parse_command(&self, src: &str) -> Result<IOCommand, ParserError>;
    fn show_help(&self) -> &[&str];
}

pub trait IIOCommandExecutor {
    fn execute_command(
        &self,
        command: IOCommand,
        right: Rights,
    ) -> Result<String, CommandExecutorError>;
}
