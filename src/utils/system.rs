use std::fs;
use std::io;
use std::io::Read;
use std::os::unix::net::UnixListener;
use std::path::Path;
use std::process::{Command, ExitStatus, Stdio};

use crate::entities::constants::{CURL, SHELL};

#[cfg(not(test))]
const COMMAND: &str = "reboot";
#[cfg(test)]
const COMMAND: &str = "__test_reboot__";

pub fn reboot() -> Result<(), String> {
    let out = Command::new(SHELL).arg("-c").arg(COMMAND).output();
    let val = match out {
        Ok(val) => val,
        Err(err) => return Err(format!("{:?}", err)),
    };
    let msg = match () {
        _ if !val.stdout.is_empty() => {
            String::from_utf8(val.stdout).unwrap_or_else(|_| String::new())
        }
        _ if !val.stderr.is_empty() => {
            String::from_utf8(val.stderr).unwrap_or_else(|_| String::new())
        }
        _ => "".to_string(),
    };
    Err(format!("reboot failed: {:?}", msg))
}

pub fn listen_socket(path: &str) -> io::Result<UnixListener> {
    if Path::new(path).exists() {
        fs::remove_file(path)?
    }
    UnixListener::bind(path)
}

fn get_pipe_data<Ch: Read>(opt_ch: Option<Ch>) -> io::Result<String> {
    let mut out = String::new();
    if let Some(mut ch) = opt_ch {
        ch.read_to_string(&mut out)?;
    }
    Ok(out)
}

pub fn execute_shell(cmd: &str) -> io::Result<(String, String, ExitStatus)> {
    let mut child = Command::new(SHELL)
        .arg("-c")
        .arg(cmd)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;
    let exit_status = child.wait()?;
    let stdout = get_pipe_data(child.stdout)?;
    let stderr = get_pipe_data(child.stderr)?;
    Ok((stdout, stderr, exit_status))
}

pub fn download_file(filename: &str, link: &str) -> io::Result<()> {
    let str_cmd = format!(
        "{curl} {url} > {filename}",
        curl = CURL,
        url = link,
        filename = filename,
    );
    let mut child = Command::new(SHELL).arg("-c").arg(str_cmd).spawn()?;
    let status = child.wait()?;
    if !status.success() {
        Err(io::Error::new(io::ErrorKind::Other, "curl failed"))
    } else {
        Ok(())
    }
}
