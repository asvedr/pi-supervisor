pub fn make_download_link(token: &str, file_path: &str) -> String {
    format!(
        "https://api.telegram.org/file/bot{token}/{file_path}",
        token = token,
        file_path = file_path,
    )
}
