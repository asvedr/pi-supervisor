use std::sync::Mutex;

use mddd::macros::singleton_simple;

use crate::entities::config::MainConfig;
use crate::impls::config_parser::ConfigParser;
use crate::proto::config::IConfigParser;

static PATH: Mutex<String> = Mutex::new(String::new());

pub fn set_path(path: String) {
    let mut guard = PATH.lock().unwrap();
    *guard = path;
}

singleton_simple!(
    pub fn get() -> MainConfig {
        let guard = PATH.lock().unwrap();
        let path: &str = &guard;
        match ConfigParser.parse_file(path) {
            Ok(val) => val,
            Err(err) => panic!("Config error: {:?}, config path: {:?}", err, path,),
        }
    }
);
