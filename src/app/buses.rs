use mddd::macros::singleton_simple;

use crate::entities::command::CoreCommand;
use crate::entities::constants::{BUS_SIZE_COMMAND, BUS_SIZE_ERRORS, BUS_SIZE_STATUS};
use crate::entities::errors::CoreProcessError;
use crate::entities::info::Status;
use crate::impls::bus::Bus;
use crate::impls::core_buses::CoreBuses;

pub type ImplBuses = CoreBuses<Bus<CoreCommand>, Bus<Status>, Bus<CoreProcessError>>;

singleton_simple!(
    pub fn get() -> ImplBuses {
        CoreBuses {
            command: Bus::new(BUS_SIZE_COMMAND),
            status: Bus::new(BUS_SIZE_STATUS),
            error: Bus::new(BUS_SIZE_ERRORS),
        }
    }
);
