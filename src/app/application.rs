use std::{mem, thread};

use crate::app::processes;
use crate::proto::core::IProcess;

pub struct Application {
    core_process: Box<dyn IProcess>,
    minor_processes: Vec<Box<dyn IProcess>>,
}

struct Wrapper {
    proc: Box<dyn IProcess>,
}

impl Wrapper {
    fn run(mut self) {
        println!("run {:?}", self.proc.name());
        self.proc.run()
    }
}

unsafe impl Send for Wrapper {}

impl Application {
    pub fn new() -> Application {
        let core_process = processes::core();
        Self {
            core_process,
            minor_processes: processes::minor_processes(),
        }
    }

    pub fn run(&mut self) {
        let handlers = mem::take(&mut self.minor_processes)
            .into_iter()
            .map(|proc| Wrapper { proc })
            .map(|w| thread::spawn(move || w.run()))
            .collect::<Vec<_>>();
        println!("run {:?} as main", self.core_process.name());
        self.core_process.run();
        for handler in handlers {
            let _ = handler.join();
        }
    }
}
