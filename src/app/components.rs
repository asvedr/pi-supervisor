use crate::app::buses;
use crate::app::buses::ImplBuses;
use crate::app::config;
use crate::impls::command_parser::CommandParser;
use crate::impls::ext_killer::ExtKiller;
use crate::impls::io_command_executor::IOCommandExecutor;
use crate::impls::notification_handlers;
use crate::impls::notification_manager::NotificationManager;
use crate::impls::proc_tree_builder::ProcTreeBuilder;
use crate::impls::start_stop::StartStop;
use crate::impls::sys_info_collector::SysInfoCollector;
use crate::impls::threshold_killer::ThresholdKiller;

pub fn info_collector() -> SysInfoCollector {
    SysInfoCollector::new(config::get())
}

pub fn start_stop() -> StartStop<ExtKiller<ProcTreeBuilder>> {
    StartStop::new(Default::default(), config::get())
}

pub fn threshold_killer() -> ThresholdKiller {
    ThresholdKiller::new(config::get())
}

pub fn command_parser() -> CommandParser {
    Default::default()
}

pub fn command_executor() -> IOCommandExecutor<ImplBuses> {
    IOCommandExecutor::new(buses::get().clone())
}

pub fn notif_manager() -> NotificationManager {
    NotificationManager::new(notification_handlers::all(), config::get())
}
