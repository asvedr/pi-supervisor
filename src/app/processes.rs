use crate::app::buses;
use crate::app::components;
use crate::app::config;
use crate::impls::core_process::CoreProcess;
use crate::impls::socket_io_process::SocketIOProcess;
use crate::impls::tg_io_process::TgIOProcess;
use crate::impls::tg_notification_process::TgNotificationProcess;
use crate::proto::core::IProcess;

pub fn core() -> Box<dyn IProcess> {
    let process = CoreProcess::new(
        components::info_collector(),
        components::start_stop(),
        buses::get().clone(),
        components::threshold_killer(),
        config::get(),
    );
    Box::new(process)
}

fn io_socket() -> Option<Box<dyn IProcess>> {
    let process = SocketIOProcess::new(
        components::command_parser(),
        components::command_executor(),
        config::get(),
    )?;
    Some(Box::new(process))
}

fn io_tg() -> Option<Box<dyn IProcess>> {
    let process = TgIOProcess::new(
        components::command_parser(),
        components::command_executor(),
        config::get(),
    )?;
    Some(Box::new(process))
}

fn notifier_tg() -> Option<Box<dyn IProcess>> {
    let process = TgNotificationProcess::new(
        components::notif_manager(),
        buses::get().clone(),
        config::get(),
    )?;
    Some(Box::new(process))
}

pub fn minor_processes() -> Vec<Box<dyn IProcess>> {
    [io_socket(), io_tg(), notifier_tg()]
        .into_iter()
        .flatten()
        .collect()
}
