# Supervisor app for raspberry pi

### Features
- start/stop processes
- kill child processes by custom rules
- system monitoring
- administration from telegram

### How to launch
Add service to autolaunch in your PI.
default config path is `/etc/pi-supervisor.yml`
minimal config:
```yaml
services:
  - name: service-name
    command: /bin/your-app -a b -c d
    start: true # Auto launch service with supervisor. Default=true
    kill_on: # Optional property. Ruless to kill service
      - temp > 38.2 # Hardware temperature
      - proc_mem > 10mb # Process memory consumption
      - free_mem < 1 gb # System free memory
  - name: other-service
    command: other-command
    env: # Optional property. Environment variables
      VAR1: VALUE1
      VAR2: VALUE2
io: # At least one IO must be configured
  - type: socket # Unix socket interface
    path: /tmp/val.sock
    timeout: 1
  - type: tg # Telegram robot interface
    admin_id: 2 # Admin telegram id
    token: abcd # Robot token
    allow_shell: false # Optional property. Allow to execute shell commands from tg. Default=false
    allow_loading: false # Optional property. Allow to download files from tg. Default=false
notificators: # Optional property. How to send notifications
  - type: tg # The only implemented method for now
    admin_id: 2 # User telegram id
    notification_interval: 1 # Minimal interval for notifications in seconds
    token: abcd # Robot token
notifications: # Optional property. Notification rules
  - type: temp > 30 # Hardware temperature
    interval: 3 # Alert interval in seconds
  - type: service_killed # One of services was automatically killed
  - type: start # Send notification on creation
  - type: cpu_usage > 1.1 # Alert on high CPU
  - type: free_mem < 10 mb # Alert on low RAM
  - type: proc_mem service_name > 10 mb # Alert on high mem consumption for service: "service_name"
  - type: bus_error # Alert if bus to core thread is broken
  - type: no_data # Alert if there is no data from core thread
sys_temp_source: /sys/class/thermal/thermal_zone0/temp # source of temperature (Optional field)
core_sleep: 1.1 # Optional field. Seconds
notificator_sleep: 2 # Optional field. Seconds
io_sleep: 3 # Optional field. Seconds
error_lifetime: 4 # Optional field. Seconds
```

### How to use
The control panel is available after the app launching via configured IO
Available commands:
- `start service-name` - start service if stopped
- `stop service-name` - stop service if started
- `stop self` - kill pi-supervisor
- `reboot` - reboot PI if command `reboot` is available
- `show sys` - show sys info(temperature, cpu, mem)
- `show all services` - show a list of available services(low info)
- `show alive` - show a list of stated services(mem, lifetime, pid, etc) 
- `show errors` - show data in error bus
- `help` - show list of commands
- `sh <your code>` - execute shell command(if `allow_shell` is true)
- <uploaded file> + <description> - save file using description as path(if `allow_loading` is true)

### Updates
If you want to update this service from TG use this tool: https://gitlab.com/asvedr/pi-updater

### Requirements
- 10mb of mem
- [optional] reboot command. (If you run robot from root, it can reboot PI via this command)
- [optional] curl. (If you want to allow file downloading)
